Bible multi langues, gratuite, hors connection, sans publicité, entièrement en anglais, français, italien, espagnol, portugais.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald.

Facile à utiliser avec des fonctions de recherches rapides, de partage, plans de lecture, Bible audio, articles, références croisées, widgets.

Fonctionne aussi sur Android TV, Chromebook.

