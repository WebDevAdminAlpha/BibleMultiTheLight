
package org.hlwd.bible;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {
    @SuppressLint("StaticFieldLeak")
    private static TabLayout tabLayout;
    private View llMain;
    private View slideViewMenu;
    private View slideViewMenuHandle;
    private View slideViewTab;
    private View slideViewTabHandleMain;
    private View slideViewTabHandle;
    private boolean isUiTelevision = false;
    private static boolean isPlanSelectAlreadyWarned = false;
    private static boolean isTodoSelectAlreadyWarned = false;
    private SCommon _s = null;

    @Override
    protected void onStart() {
        try {
            super.onStart();

            CheckLocalInstance(getApplicationContext());

            _s.DeleteAllLogs();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            CheckLocalInstance(getApplicationContext());

            if (PCommon._isDebugVersion) System.out.println("Main: onCreate");

            Create(true);
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void Create(final boolean isOnCreate) {
        try {
            if (PCommon._isDebugVersion) System.out.println("Main: Create");

            CheckLocalInstance(getApplicationContext());

            PCommon.SetLocale(MainActivity.this);

            isUiTelevision = PCommon.IsUiTelevision(getApplicationContext());

            if (isOnCreate) {
                final int themeId = PCommon.GetPrefThemeId(getApplicationContext());
                Tab.isThemeWhite = PCommon.GetPrefThemeName(getApplicationContext()).compareTo("LIGHT") == 0;
                setTheme(themeId);
            }

            setContentView(PCommon.SetUILayout(getApplicationContext(), R.layout.activity_main, R.layout.activity_main_tv));

            llMain = findViewById(isUiTelevision ? R.id.slideViewTab : R.id.llMain);
            slideViewMenu = (isUiTelevision) ? findViewById(R.id.svSlideViewMenu) : null;
            slideViewMenuHandle = (isUiTelevision) ? findViewById(R.id.mnuTvHandle) : null;
            slideViewTab = (isUiTelevision) ? findViewById(R.id.slideViewTab) : null;
            slideViewTabHandleMain = (isUiTelevision) ? findViewById(R.id.slideViewTabHandleMain) : null;
            slideViewTabHandle = (isUiTelevision) ? findViewById(R.id.slideViewTabHandle) : null;

            if (!isUiTelevision) {
                final Toolbar toolbar = findViewById(R.id.toolbar);
                if (toolbar != null) {
                    setSupportActionBar(toolbar);

                    if (PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.UI_LAYOUT, "C").equalsIgnoreCase("L")) {
                        final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
                        params.setScrollFlags(0);
                    }
                }
            } else {
                final Context context = getApplicationContext();
                final Typeface typeface = PCommon.GetTypeface(context);
                final int fontSize = PCommon.GetFontSize(context);
                final String bordersDialog = PCommon.GetUiLayoutTVBorders(getApplicationContext(), IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS);
                String[] borders = bordersDialog.split(",");
                if (borders.length != 4)
                    borders = PCommon.GetUiLayoutTVBorders(getApplicationContext(), null).split(",");

                int left = Integer.parseInt(borders[0]);
                left = PCommon.ConvertDpToPx(context, left);
                int top = Integer.parseInt(borders[1]);
                top = PCommon.ConvertDpToPx(context, top);
                int right = Integer.parseInt(borders[2]);
                right = PCommon.ConvertDpToPx(context, right);
                int bottom = Integer.parseInt(borders[3]);
                bottom = PCommon.ConvertDpToPx(context, bottom);

                final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                layoutParams.setMargins(left, top, right, bottom);

                final RelativeLayout rlOverscan = findViewById(R.id.rlOverscan);
                rlOverscan.setLayoutParams(layoutParams);

                if (slideViewMenuHandle != null) {
                    slideViewMenuHandle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Slide(false);
                        }
                    });
                }
                if (slideViewTabHandle != null) {
                    slideViewTabHandle.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Slide(true);
                        }
                    });
                }
                final View slideViewTabSearch = findViewById(R.id.slideViewTabSearch);
                slideViewTabSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SearchDialog(v.getContext(), true);
                    }
                });
                final View slideViewTabFilter = findViewById(R.id.slideViewTabFilter);
                slideViewTabFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SearchDialog(v.getContext(), false);
                    }
                });
                final View slideViewTabBooks = findViewById(R.id.slideViewTabBooks);
                slideViewTabBooks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShowBooks(v.getContext());
                    }
                });
                final View slideViewTabPrbl = findViewById(R.id.slideViewTabPrbl);
                slideViewTabPrbl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShowPrbl();
                    }
                });
                final View slideViewTabArticles = findViewById(R.id.slideViewTabArticles);
                slideViewTabArticles.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PCommon.ShowArticles(v.getContext());
                    }
                });
                final TextView mnuTvArticles = findViewById(R.id.mnuTvArticles);
                mnuTvArticles.setTextSize(fontSize);
                if (typeface != null) mnuTvArticles.setTypeface(typeface);
                mnuTvArticles.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        PCommon.ShowArticles(v.getContext());
                    }
                });
                final TextView mnuTvBooks = findViewById(R.id.mnuTvBooks);
                mnuTvBooks.setTextSize(fontSize);
                if (typeface != null) mnuTvBooks.setTypeface(typeface);
                mnuTvBooks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        ShowBooks(v.getContext());
                    }
                });
                final TextView mnuTvFav = findViewById(R.id.mnuTvFav);
                mnuTvFav.setTextSize(fontSize);
                if (typeface != null) mnuTvFav.setTypeface(typeface);
                mnuTvFav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        ShowFav();
                    }
                });
                final TextView mnuTvHelp = findViewById(R.id.mnuTvHelp);
                mnuTvHelp.setTextSize(fontSize);
                if (typeface != null) mnuTvHelp.setTypeface(typeface);
                mnuTvHelp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        PCommon.ShowArticle(v.getContext(), "ART_APP_HELP");
                    }
                });
                final TextView mnuTvTools = findViewById(R.id.mnuTvTools);
                mnuTvTools.setTextSize(fontSize);
                if (typeface != null) mnuTvTools.setTypeface(typeface);
                mnuTvTools.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        ShowTools(v.getContext());
                    }
                });
                final TextView mnuTvTodos = findViewById(R.id.mnuTvTodos);
                mnuTvTodos.setTextSize(fontSize);
                if (typeface != null) mnuTvTodos.setTypeface(typeface);
                mnuTvTodos.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        ShowTodos();
                    }
                });
                final TextView mnuTvPlans = findViewById(R.id.mnuTvPlans);
                mnuTvPlans.setTextSize(fontSize);
                if (typeface != null) mnuTvPlans.setTypeface(typeface);
                mnuTvPlans.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        ShowPlans();
                    }
                });
                final TextView mnuTvPrbl = findViewById(R.id.mnuTvPrbl);
                mnuTvPrbl.setTextSize(fontSize);
                if (typeface != null) mnuTvPrbl.setTypeface(typeface);
                mnuTvPrbl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        ShowPrbl();
                    }
                });
                final TextView mnuTvSettings = findViewById(R.id.mnuTvSettings);
                mnuTvSettings.setTextSize(fontSize);
                if (typeface != null) mnuTvSettings.setTypeface(typeface);
                mnuTvSettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Slide(false);
                        final Intent intent = new Intent(getApplicationContext(), PreferencesActivity.class);
                        startActivityForResult(intent, 1);
                    }
                });
                final TextView mnuTvInvite = findViewById(R.id.mnuTvInvite);
                mnuTvInvite.setTextSize(fontSize);
                if (typeface != null) mnuTvInvite.setTypeface(typeface);
                mnuTvInvite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InviteFriend();
                    }
                });
                final TextView mnuTvAbout = findViewById(R.id.mnuTvAbout);
                mnuTvAbout.setTextSize(fontSize);
                if (typeface != null) mnuTvAbout.setTypeface(typeface);
                mnuTvAbout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShowAbout(v.getContext());
                    }
                });
                final TextView mnuTvQuit = findViewById(R.id.mnuTvQuit);
                mnuTvQuit.setTextSize(fontSize);
                if (typeface != null) mnuTvQuit.setTypeface(typeface);
                mnuTvQuit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PCommon.TryQuitApplication(v.getContext());
                    }
                });
            }

            tabLayout = findViewById(R.id.tabLayout);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(final TabLayout.Tab tab) {
                    try {
                        final int tabId = tab.getPosition();
                        if (PCommon._isDebugVersion) System.out.println("TabSelected: " + tabId);

                        final CacheTabBO cacheTab = _s.GetCacheTab(tabId);
                        final SearchFragment.FRAGMENT_TYPE fragmentType;

                        final FragmentManager fm = getSupportFragmentManager();
                        final FragmentTransaction ft = fm.beginTransaction();

                        if (cacheTab == null) {
                            fragmentType = SearchFragment.FRAGMENT_TYPE.SEARCH_TYPE;
                        } else {
                            if (cacheTab.tabType.compareTo("S") == 0) {
                                fragmentType = SearchFragment.FRAGMENT_TYPE.SEARCH_TYPE;
                            } else if (cacheTab.tabType.compareTo("F") == 0) {
                                fragmentType = SearchFragment.FRAGMENT_TYPE.FAV_TYPE;
                            } else if (cacheTab.tabType.compareTo("P") == 0) {
                                fragmentType = SearchFragment.FRAGMENT_TYPE.PLAN_TYPE;
                            } else {
                                fragmentType = SearchFragment.FRAGMENT_TYPE.ARTICLE_TYPE;
                            }
                        }
                        final Fragment frag = new SearchFragment(fragmentType);
                        ft.replace(R.id.content_frame, frag, Integer.toString(tabId));
                            /* Gives exceptions in Android Q
                            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                            */
                        ft.commit();

                        if (isUiTelevision) {
                            final int INSTALL_STATUS = PCommon.GetInstallStatus(getApplicationContext());
                            if (INSTALL_STATUS != 6) {
                                final int perc = GetInstallStatusPerc();
                                if (perc < 10) return;
                                final String contentMsg = GetInstallStatusMsg();
                                PCommon.ShowToast(getApplicationContext(), contentMsg, Toast.LENGTH_SHORT);
                            }
                        }
                    } catch (Exception ex) {
                        if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
                    } finally {
                        Tab.LongPress(tabLayout.getContext());
                    }
                }

                @Override
                public void onTabUnselected(final TabLayout.Tab tab) {
                    try {
                        if (tab == null)
                            return;

                        final int tabId = tab.getPosition();
                        if (tabId < 0)
                            return;

                        final FragmentManager fm = getSupportFragmentManager();
                        final Fragment frag = fm.findFragmentByTag(Integer.toString(tabId));
                        if (frag == null)
                            return;
                        if (!(frag instanceof SearchFragment))
                            return;

                        final int posY = SearchFragment.GetScrollPosY();
                        final CacheTabBO t = _s.GetCacheTab(tabId);
                        if (t == null)
                            return;

                        t.scrollPosY = posY;
                        _s.SaveCacheTab(t);
                    } catch (Exception ex) {
                        if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
                    }
                }

                @Override
                public void onTabReselected(final TabLayout.Tab tab) {
                }
            });

            tabLayout.post(new Runnable() {
                @Override
                public void run() {
                    TabLayout.Tab tab;
                    String tabTitle;
                    final int tabCount = _s.GetCacheTabCount();
                    for (int i = 0; i < tabCount; i++) {
                        tab = tabLayout.newTab().setText(Integer.toString(i));
                        tabLayout.addTab(tab);

                        tabTitle = _s.GetCacheTabTitle(i);
                        if (tabTitle == null) tabTitle = getString(R.string.tabTitleDefault);
                        tab.setText(tabTitle);
                    }

                    final int restoreTabSelected = Integer.parseInt(PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.TAB_SELECTED, "0"));
                    if (restoreTabSelected >= 0) {
                        if (tabLayout != null) {
                            if (tabCount > 0 && restoreTabSelected < tabCount) {
                                tabLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //noinspection EmptyCatchBlock
                                        try {
                                            //noinspection ConstantConditions
                                            tabLayout.getTabAt(restoreTabSelected).select();
                                        } catch (Exception ex) {
                                        }
                                    }
                                });
                            }
                        }
                    }

                    final int UPDATE_STATUS = Integer.parseInt(PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.UPDATE_STATUS, "1"));
                    if (UPDATE_STATUS != 1) {
                        PCommon.SavePrefInt(getApplicationContext(), IProject.APP_PREF_KEY.UPDATE_STATUS, 1);
                        PCommon.ShowArticle(getApplicationContext(), "ART_APP_LOG");
                    }
                }
            });
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    protected void onPostResume() {
        try {
            super.onPostResume();

            if (PCommon._isDebugVersion) System.out.println("Main: onPostResume");

            final String BIBLE_NAME = PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME, "");
            if (BIBLE_NAME.compareToIgnoreCase("k") != 0 && BIBLE_NAME.compareToIgnoreCase("l") != 0 && BIBLE_NAME.compareToIgnoreCase("d") != 0 && BIBLE_NAME.compareToIgnoreCase("v") != 0 && BIBLE_NAME.compareToIgnoreCase("a") != 0 && BIBLE_NAME.compareToIgnoreCase("o") != 0) {
                //Forced temporary
                PCommon.SavePref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME, "k");

                final LayoutInflater inflater = getLayoutInflater();
                final View view = inflater.inflate(R.layout.fragment_languages, (ViewGroup) findViewById(R.id.llLanguages));
                final String msg = getString(R.string.mnuLanguage);
                final String desc = "";
                final AlertDialog builder = new AlertDialog.Builder(MainActivity.this).create();
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final Handler hdl = new Handler();
                        hdl.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                final String bbName = PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, "k");
                                PCommon.SavePref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME, bbName);

                                PCommon.SetLocale(MainActivity.this);

                                PCommon.ShowArticle(getApplicationContext(), "ART_APP_LOG");

                                PCommon.ShowArticle(getApplicationContext(), "ART_APP_HELP");

                                PCommon.SavePrefInt(getApplicationContext(), IProject.APP_PREF_KEY.TAB_SELECTED, 2);

                                PCommon.ShowDialog(MainActivity.this, R.string.languageInstalling, false, R.string.installMsg);

                                Recreate();
                            }
                        }, 0);
                    }
                });

                PCommon.SelectBibleLanguage(builder, getApplicationContext(), view, msg, desc, false, true);
                builder.show();
            }
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    public void onPause() {
        try {
            super.onPause();

            Tab.SaveScrollPosY(getApplicationContext());
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.menu_bible, menu);

            if (!isUiTelevision) {
                final int INSTALL_STATUS = PCommon.GetInstallStatus(getApplicationContext());
                if (INSTALL_STATUS != 6) {
                    menu.findItem(R.id.mnu_prbl).setEnabled(false);
                    menu.findItem(R.id.mnu_articles).setEnabled(false);
                    menu.findItem(R.id.mnu_plans).setEnabled(false);
                    menu.findItem(R.id.mnu_todos).setEnabled(false);
                    menu.findItem(R.id.mnu_tools).setEnabled(false);
                    menu.findItem(R.id.mnu_invite).setEnabled(false);
                    menu.findItem(R.id.mnu_group_settings).setEnabled(false);
                    final MenuItem mnu_search_prbl = menu.findItem(R.id.mnu_search_prbl);
                    if (mnu_search_prbl != null) mnu_search_prbl.setEnabled(false);

                    final String contentMsg = GetInstallStatusMsg();
                    final Snackbar snackbar = Snackbar.make(llMain, contentMsg, Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
        try {
            final int id = item.getItemId();
            switch (id) {
                case R.id.mnu_add_tab:

                    Tab.AddTab(getApplicationContext());
                    return true;

                case R.id.mnu_remove_tab:

                    Tab.RemoveCurrentTab(getApplicationContext());
                    return true;

                case R.id.mnu_show_fav:

                    ShowFav();
                    return true;

                case R.id.mnu_plan:

                    final int planId = Integer.parseInt(PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.PLAN_ID, "-1"));
                    final int planPageNumber = Integer.parseInt(PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.PLAN_PAGE, "-1"));
                    ShowPlan(planId, planPageNumber);
                    return true;

                case R.id.mnu_books:

                    ShowBooks(this);
                    return true;

                case R.id.mnu_plans:

                    ShowPlans();
                    return true;

                case R.id.mnu_todos:

                    ShowTodos();
                    return true;

                case R.id.mnu_prbl:
                case R.id.mnu_search_prbl:

                    ShowPrbl();
                    return true;

                case R.id.mnu_articles:

                    PCommon.ShowArticles(this);
                    return true;

                case R.id.mnu_tools:

                    ShowTools(this);
                    return true;

                case R.id.mnu_group_settings:

                    final Intent intent = new Intent(getApplicationContext(), PreferencesActivity.class);
                    startActivityForResult(intent, 1);

                    return true;

                case R.id.mnu_help:

                    PCommon.ShowArticle(this, "ART_APP_HELP");
                    return true;

                case R.id.mnu_invite:

                    InviteFriend();
                    return true;

                case R.id.mnu_about:

                    ShowAbout(this);
                    return true;

                case R.id.mnu_quit:

                    final int status = PCommon.TryQuitApplication(getApplicationContext());
                    if (status == 0) finishAffinity();
                    return true;
            }
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        try {
            super.onDestroy();

            if (tabLayout != null) {
                tabLayout.removeAllTabs();
                tabLayout.removeAllViews();
                tabLayout = null;
            }

            _s = null;
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        try {
            super.onConfigurationChanged(newConfig);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Tab.SaveScrollPosY(getApplicationContext());

                    Recreate();
                }
            }, 0);
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {
                Recreate();
            } else {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isUiTelevision) {
                            final String contentMsg = getString(R.string.toastRestartLong);
                            final int waitDuration = Integer.parseInt(getString(R.string.snackbarWaitVeryLong));
                            final Snackbar snackbar = Snackbar.make(llMain, contentMsg, waitDuration);
                            snackbar.show();
                        } else {
                            PCommon.ShowToast(getApplicationContext(), R.string.toastRestartShort, Toast.LENGTH_LONG);
                        }
                    }
                }, 500);
            }
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Simulate recreate()
     */
    private void Recreate() {
        try {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (PCommon._isDebugVersion) System.out.println("Main: ReCreate");

                    Create(false);

                    final int tabId = Tab.GetCurrentTabPosition();
                    if (tabId >= 0) {
                        Objects.requireNonNull(tabLayout.getTabAt(tabId)).select();
                    }
                }
            }, 0);
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance(final Context context) {
        try {
            if (_s == null) {
                _s = SCommon.GetInstance(context);
            }
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    /***
     * Get install status percentage
     * @return percentage
     */
    private int GetInstallStatusPerc() {
        final float pos = _s.GetBibleIdCount();

        return Math.round((pos * 100) / 186612);
    }

    /***
     * Get install status msg
     * @return Content msg
     */
    private String GetInstallStatusMsg() {
        final int perc = GetInstallStatusPerc();

        return PCommon.ConcaT(getString(R.string.languageInstalling), " (", perc == 0 ? 1 : perc, "%)");
    }

    /***
     * Show Fav (open Fav or goto it)
     */
    private void ShowFav() {
        try {
            if (tabLayout == null) return;

            final int tabCount = tabLayout.getTabCount();
            @SuppressWarnings("UnusedAssignment") boolean isFavShow = false;

            CacheTabBO cacheTabFav = _s.GetCacheTabFav();
            if (cacheTabFav == null) {
                //noinspection ConstantConditions
                isFavShow = false;

                cacheTabFav = new CacheTabBO();
                cacheTabFav.tabNumber = -1;
                cacheTabFav.tabType = "F";
                cacheTabFav.tabTitle = getString(R.string.favHeader);

                _s.SaveCacheTabFav(cacheTabFav);
            } else {
                isFavShow = (cacheTabFav.tabNumber >= 0);
            }

            isFavShow = !isFavShow;
            if (isFavShow) {
                //Show fav tab
                //############
                for (int i = tabCount - 1; i >= 0; i--) {
                    _s.UpdateCacheId(i, i + 1);
                }

                cacheTabFav.tabNumber = 0;
                _s.SaveCacheTabFav(cacheTabFav);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.favHeader);
                tabLayout.addTab(tab, 0);
            }
            Tab.FullScrollTab(getApplicationContext(), HorizontalScrollView.FOCUS_LEFT);
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowBooks(final Context context) {
        try {
            CheckLocalInstance(context);

            final int installStatus = PCommon.GetInstallStatus(context);
            if (installStatus < 1) return;

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final String bbnm = PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_NAME, "k");

            final AlertDialog builderBook = new AlertDialog.Builder(context).create();
            final LayoutInflater inflater = getLayoutInflater();

            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final ArrayList<BibleRefBO> lstRef = _s.GetListAllBookByName(bbnm);
            final LinearLayout llBooks = new LinearLayout(context);
            llBooks.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llBooks.setOrientation(LinearLayout.VERTICAL);
            llBooks.setPadding(20, 20, 20, 20);

            final AlertDialog builderChapter = new AlertDialog.Builder(context).create();             //, R.style.DialogStyleKaki
            final View vwSvSelection = inflater.inflate(R.layout.fragment_selection_items, (ViewGroup) findViewById(R.id.svSelection));

            final AlertDialog builderLanguages = new AlertDialog.Builder(context).create();
            final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, (ViewGroup) findViewById(R.id.llLanguages));

            int bNumber;
            String refText;
            String refNr;
            boolean isBookExist;
            int bNumberParam;
            boolean shouldWarn = false;

            for (BibleRefBO ref : lstRef) {
                bNumber = ref.bNumber;
                refNr = String.format(Locale.US, "%2d", bNumber);
                refText = PCommon.ConcaT(refNr, Html.fromHtml("&nbsp;"), "(", ref.bsName, ") ", ref.bName);

                final TextView tvBook = new TextView(context);
                tvBook.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvBook.setPadding(10, 20, 10, 20);
                tvBook.setText(refText);
                tvBook.setTag(bNumber);

                bNumberParam = (bNumber != 66) ? bNumber + 1 : 66;
                isBookExist = (installStatus == 6) || _s.IsBookExist(bNumberParam);
                if (isBookExist) {
                    tvBook.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View view) {
                            try {
                                final int bNumber = (int) view.getTag();
                                if (PCommon._isDebugVersion) System.out.println(bNumber);

                                final int chapterMax = _s.GetBookChapterMax(bNumber);
                                if (chapterMax < 1) {
                                    PCommon.ShowToast(view.getContext(), R.string.toastBookNotInstalled, Toast.LENGTH_SHORT);
                                    return;
                                }
                                final String[] titleArr = ((TextView) view).getText().toString().substring(3).split("\\(");
                                final String title = PCommon.ConcaT(getString(R.string.mnuBook), ": ", titleArr[0]);

                                PCommon.SelectItem(builderChapter, view.getContext(), vwSvSelection, title, R.string.tvChapter, "", true, chapterMax, false);
                                builderChapter.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        PCommon.SelectBibleLanguageMulti(builderLanguages, view.getContext(), vllLanguages, title, "", true, false);
                                        builderLanguages.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override
                                            public void onDismiss(DialogInterface dialogInterface) {
                                                final String bbName = PCommon.GetPrefBibleName(context);
                                                final String bbname = PCommon.GetPref(view.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                                                if (bbname.equals("")) return;
                                                final String tbbName = PCommon.GetPrefTradBibleName(view.getContext(), true);
                                                final int cNumber = Integer.parseInt(PCommon.GetPref(view.getContext(), IProject.APP_PREF_KEY.BOOK_CHAPTER_DIALOG, "1"));
                                                final String fullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                                                MainActivity.Tab.AddTab(view.getContext(), "S", tbbName, fullQuery);
                                            }
                                        });
                                        builderLanguages.show();
                                    }
                                });
                                builderChapter.show();
                            } catch (Exception ex) {
                                if (PCommon._isDebugVersion) PCommon.LogR(view.getContext(), ex);
                            } finally {
                                builderBook.dismiss();
                            }
                        }
                    });
                } else {
                    if (!shouldWarn) shouldWarn = true;
                    tvBook.setEnabled(false);
                }
                //TODO FAB: slow GetDrawable
                tvBook.setFocusable(true);
                tvBook.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) tvBook.setTypeface(typeface);
                tvBook.setTextSize(fontSize);

                llBooks.addView(tvBook);
            }

            final TextView tvNT = new TextView(context);
            tvNT.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvNT.setPadding(20, 60, 20, 20);
            tvNT.setGravity(Gravity.CENTER_HORIZONTAL);
            tvNT.setText(Html.fromHtml(context.getString(R.string.tvBookNT)));
            PCommon.SetTextAppareance(tvNT, context, R.style.TextAppearance_AppCompat_Headline);
            if (typeface != null) tvNT.setTypeface(typeface);
            llBooks.addView(tvNT, 39);

            final TextView tvOT = new TextView(context);
            tvOT.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvOT.setPadding(20, 20, 20, 20);
            tvOT.setGravity(Gravity.CENTER_HORIZONTAL);
            tvOT.setText(Html.fromHtml(context.getString(R.string.tvBookOT)));
            PCommon.SetTextAppareance(tvOT, context, R.style.TextAppearance_AppCompat_Headline);
            if (typeface != null) tvOT.setTypeface(typeface);

            llBooks.addView(tvOT, 0);

            if (shouldWarn) {
                final TextView tvWarn = new TextView(context);
                tvWarn.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvWarn.setPadding(10, 10, 10, 20);
                tvWarn.setGravity(Gravity.CENTER_HORIZONTAL);
                tvWarn.setText(R.string.tvBookInstall);
                tvWarn.setTextSize(fontSize);
                llBooks.addView(tvWarn, 0);
            }
            sv.addView(llBooks);

            builderBook.setTitle(R.string.mnuBooks);
            builderBook.setCancelable(true);
            builderBook.setView(sv);
            builderBook.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    private void ShowPrbl() {
        try {
            CheckLocalInstance(getApplicationContext());

            final AlertDialog builder = new AlertDialog.Builder(this).create();                     //R.style.DialogStyleKaki
            final ScrollView sv = new ScrollView(this);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final LinearLayout llPrbl = new LinearLayout(this);
            llPrbl.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llPrbl.setOrientation(LinearLayout.VERTICAL);
            llPrbl.setPadding(20, 20, 20, 20);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            int resId;
            String[] prblValue;
            String[] prblBookValue;
            BibleRefBO bookRef;
            TextView tvPrbl;
            String text;
            String bsName;

            final String bbName = PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME, "k");
            final AlertDialog builderLanguages = new AlertDialog.Builder(this).create();             //, R.style.DialogStyleKaki
            final LayoutInflater inflater = getLayoutInflater();
            final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, (ViewGroup) findViewById(R.id.llLanguages));
            final Spanned htmlSpace = Html.fromHtml("&nbsp;");

            for (String prblRef : this.getResources().getStringArray(R.array.PRBL_ARRAY)) {
                prblValue = prblRef.split("\\|");
                prblBookValue = prblValue[1].split(" ");
                bookRef = _s.GetBookRef(bbName, Integer.parseInt(prblBookValue[0]));

                resId = PCommon.GetResId(this, prblValue[0]);
                text = PCommon.ConcaT(getString(R.string.bulletDefault), htmlSpace, "(", bookRef.bsName, ")", htmlSpace, getString(resId));

                tvPrbl = new TextView(this);
                tvPrbl.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvPrbl.setPadding(20, 20, 20, 20);
                tvPrbl.setText(text);
                tvPrbl.setTag(prblValue[1]);
                tvPrbl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        try {
                            final String fullQuery = (String) view.getTag();
                            if (PCommon._isDebugVersion) System.out.println(fullQuery);

                            final String msg = ((TextView) view).getText().toString().substring(2);
                            PCommon.SelectBibleLanguageMulti(builderLanguages, view.getContext(), vllLanguages, msg, "", true, false);
                            builderLanguages.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    final String bbname = PCommon.GetPref(view.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                                    if (bbname.equals("")) return;
                                    final String tbbName = PCommon.GetPrefTradBibleName(view.getContext(), true);
                                    MainActivity.Tab.AddTab(view.getContext(), "S", tbbName, fullQuery);
                                }
                            });
                            builderLanguages.show();
                        } catch (Exception ex) {
                            if (PCommon._isDebugVersion) PCommon.LogR(view.getContext(), ex);
                        } finally {
                            builder.dismiss();
                        }
                    }
                });
                tvPrbl.setFocusable(true);
                tvPrbl.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                //Font
                if (typeface != null) tvPrbl.setTypeface(typeface);
                tvPrbl.setTextSize(fontSize);

                llPrbl.addView(tvPrbl);
            }
            sv.addView(llPrbl);

            builder.setTitle(R.string.mnuPrbl);
            builder.setCancelable(true);
            builder.setView(sv);
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private TextView CreateTvTitle(final Context context, final int titleId, final Typeface typeface) {
        try {
            final TextView tvTitle = new TextView(context);
            tvTitle.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvTitle.setPadding(20, 100, 20, 20);
            tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            tvTitle.setText(Html.fromHtml(context.getString(titleId)));
            PCommon.SetTextAppareance(tvTitle, context, R.style.TextAppearance_AppCompat_Headline);
            if (typeface != null) tvTitle.setTypeface(typeface);

            return tvTitle;
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }

        return null;
    }

    private void ShowPlans() {
        try {
            CheckLocalInstance(this);

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            final ScrollView sv = new ScrollView(this);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final LinearLayout llPlans = new LinearLayout(this);
            llPlans.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llPlans.setOrientation(LinearLayout.VERTICAL);
            llPlans.setPadding(20, 20, 20, 20);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            TextView tvPlan, tvTitle;

            int resId, idx;
            String[] cols;
            String text;

            idx = 0;
            for (String plan : this.getResources().getStringArray(R.array.PLAN_ARRAY)) {
                cols = plan.split("\\|");
                final String planRef = cols[0];
                final boolean planExist = _s.IsPlanDescExist(planRef);

                resId = PCommon.GetResId(this, planRef);
                text = PCommon.ConcaT(getString(R.string.bulletDefault), Html.fromHtml("&nbsp;"), getString(resId));

                tvPlan = new TextView(this);
                tvPlan.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvPlan.setPadding(20, 20, 20, 20);
                tvPlan.setText(text);
                tvPlan.setTag(idx);
                tvPlan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        try {
                            final int planIdx = Integer.parseInt(view.getTag().toString());
                            ShowPlan(true, planRef, planIdx, -1, -1);
                        } catch (Exception ex) {
                            if (PCommon._isDebugVersion) PCommon.LogR(view.getContext(), ex);
                        } finally {
                            builder.dismiss();
                        }
                    }
                });
                if (planExist) {
                    tvPlan.setEnabled(false);
                    tvPlan.setTextColor(ContextCompat.getColor(this, R.color.colorDisable));
                }
                tvPlan.setFocusable(true);
                tvPlan.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                //Font
                if (typeface != null) tvPlan.setTypeface(typeface);
                tvPlan.setTextSize(fontSize);

                llPlans.addView(tvPlan);
                idx++;
            }
            tvTitle = CreateTvTitle(this, R.string.tvPlanThemes, typeface);
            if (tvTitle != null) llPlans.addView(tvTitle, 0);
            tvTitle = CreateTvTitle(this, R.string.tvPlanBooks, typeface);
            if (tvTitle != null) {
                tvTitle.setPadding(tvTitle.getPaddingLeft(), tvTitle.getPaddingTop() + 40, tvTitle.getPaddingRight(), tvTitle.getPaddingBottom());
                llPlans.addView(tvTitle, 10);
            }

            //~~~
            final int planDescIdMax = _s.GetPlanDescIdMax();
            if (planDescIdMax <= 0) {
                if (!isPlanSelectAlreadyWarned) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isPlanSelectAlreadyWarned = true;
                            PCommon.ShowDialog(MainActivity.this, R.string.planSelect, false, R.string.planSelectMsg);
                        }
                    }, 500);
                }
            } else {
                final GridLayout glYourPlans = new GridLayout(this);
                glYourPlans.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                glYourPlans.setColumnCount(1);
                llPlans.addView(glYourPlans, 0);

                ArrayList<PlanDescBO> lstPd = _s.GetAllPlanDesc();
                for (PlanDescBO pd : lstPd) {
                    idx = 0;
                    @SuppressWarnings("UnusedAssignment") String plan = null;

                    for (String planToFind : this.getResources().getStringArray(R.array.PLAN_ARRAY)) {
                        cols = planToFind.split("\\|");
                        if (cols[0].compareTo(pd.planRef) == 0) {
                            final int planId = pd.planId;
                            plan = planToFind;

                            cols = plan.split("\\|");
                            final String planRef = cols[0];
                            resId = PCommon.GetResId(this, planRef);
                            text = PCommon.ConcaT(getString(R.string.bulletDefault),  Html.fromHtml("&nbsp;"), getString(resId));

                            final HorizontalScrollView hsv = new HorizontalScrollView(this);
                            hsv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

                            tvPlan = new TextView(this);
                            tvPlan.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                            tvPlan.setPadding(20, 10, 20, 0);
                            tvPlan.setText(text);
                            tvPlan.setTag(idx);
                            tvPlan.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(final View view) {
                                    try {
                                        final int planIdx = Integer.parseInt(view.getTag().toString());
                                        if (PCommon._isDebugVersion) System.out.println(planIdx);
                                        ShowPlan(false, planRef, planIdx, planId, -1);
                                    } catch (Exception ex) {
                                        if (PCommon._isDebugVersion)
                                            PCommon.LogR(view.getContext(), ex);
                                    } finally {
                                        builder.dismiss();
                                    }
                                }
                            });
                            tvPlan.setFocusable(true);
                            tvPlan.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                            text = _s.GetPlanCalProgressStatus(planId);
                            final TextView tvStatus = new TextView(this);
                            tvStatus.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                            tvStatus.setPadding(50, 10, 20, 0);
                            tvStatus.setText(Html.fromHtml(text));
                            tvStatus.setTag(idx);
                            tvStatus.setFocusable(true);
                            tvStatus.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));
                            tvStatus.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(final View view) {
                                    try {
                                        final int planIdx = Integer.parseInt(view.getTag().toString());
                                        if (PCommon._isDebugVersion) System.out.println(planIdx);
                                        ShowPlan(false, planRef, planIdx, planId, -1);
                                    } catch (Exception ex) {
                                        if (PCommon._isDebugVersion)
                                            PCommon.LogR(view.getContext(), ex);
                                    } finally {
                                        builder.dismiss();
                                    }
                                }
                            });
                            tvStatus.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View view) {
                                    ShowPlansMenu(builder, planId);
                                    return false;
                                }
                            });
                            //Font
                            if (typeface != null) {
                                tvPlan.setTypeface(typeface);
                                tvStatus.setTypeface(typeface);
                            }
                            tvPlan.setTextSize(fontSize);
                            tvStatus.setTextSize(fontSize);

                            hsv.addView(tvStatus);
                            glYourPlans.addView(hsv, 0);
                            glYourPlans.addView(tvPlan, 0);
                        }
                        idx++;
                    }
                }
                if (lstPd.size() > 0) lstPd.clear();
            }
            tvTitle = CreateTvTitle(this, R.string.tvPlanYourPlans, typeface);
            if (tvTitle != null) llPlans.addView(tvTitle, 0);

            //~~~
            sv.addView(llPlans);

            builder.setTitle(R.string.mnuPlansReading);
            builder.setCancelable(true);
            builder.setView(sv);
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Show plans context menu
     * @param dlgPlans  Parent dialog
     * @param planId    Plan Id
     */
    private void ShowPlansMenu(final AlertDialog dlgPlans, final int planId) {
        try {
            CheckLocalInstance(this);

            final PlanDescBO pd = _s.GetPlanDesc(planId);
            if (pd == null) return;

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_plan_delete_menu, (ViewGroup) this.findViewById(R.id.llToolsMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuPlanReading);
            builder.setView(view);

            final int resId = PCommon.GetResId(getApplicationContext(), pd.planRef);
            final String planTitle = PCommon.ConcaT("<b>", getString(resId), ":</b>");
            final TextView tvPlanTitle = view.findViewById(R.id.tvToolsTitle);
            tvPlanTitle.setText(Html.fromHtml(planTitle));
            if (typeface != null) tvPlanTitle.setTypeface(typeface);
            tvPlanTitle.setTextSize(fontSize);

            final Button btnDelete = view.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnDelete.setEnabled(false);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            _s.DeletePlan(planId);
                            builder.dismiss();
                            dlgPlans.dismiss();
                            ShowPlans();
                        }
                    }, 500);
                }
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Show plan
     * @param planId        Plan Id
     * @param pageNumber    Page number (should be >= 0 (0 = first page))
     */
    private void ShowPlan(final int planId, final int pageNumber) {
        try {
            CheckLocalInstance(getApplicationContext());

            final PlanDescBO pd = _s.GetPlanDesc(planId);
            final boolean isNewPlan = pd == null;
            if (isNewPlan) return;

            int idx = 0, planIdx = -1;
            String[] cols;

            for (String planToFind : this.getResources().getStringArray(R.array.PLAN_ARRAY)) {
                cols = planToFind.split("\\|");
                if (cols[0].compareTo(pd.planRef) == 0) {
                    planIdx = idx;
                    break;
                }

                idx++;
            }
            if (planIdx < 0) return;

            //noinspection ConstantConditions
            ShowPlan(isNewPlan, pd.planRef, planIdx, planId, pageNumber);
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Divide a / b
     * @param a (nomin)
     * @param b (denomin)
     * @return result or result + 1
     */
    private int Div(final int a, final int b) {
        return a % b == 0 ? a / b : (a / b) + 1;
    }

    /***
     * Show plan
     * @param isNewPlan     True: new plan, False: Update
     * @param planRef       Plan reference
     * @param planIdx       Position in array of templates
     * @param planId        Plan Id
     * @param pageNumber    Page number (should be >= 0 (0 = first page), -1 = auto)
     */
    @SuppressWarnings("UnusedAssignment")
    private void ShowPlan(final boolean isNewPlan, final String planRef, final int planIdx, final int planId, final int pageNumber) {
        try {
            CheckLocalInstance(this);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            //Check PageNumber
            final String bbname = PCommon.GetPrefBibleName(this);
            final int planCalRowCount = _s.GetPlanCalRowCount(bbname, planId);
            final int pageSize = 31;
            final int pageCount = ((planCalRowCount / pageSize) + 1);
            final int fpageNumber;
            if (pageNumber <= 0) {
                //TODO find dayNumber
                fpageNumber = 0;
            } else if (pageNumber >= pageCount) {
                fpageNumber = pageCount - 1;
            } else {
                fpageNumber = pageNumber;
            }

            final String plan = getApplicationContext().getResources().getStringArray(R.array.PLAN_ARRAY)[planIdx];
            final String[] cols = plan.split("\\|");
            final int bCount, cCount, vCount;
            int bNumber;
            final PlanDescBO pd;
            final String dtFormat = "yyyyMMdd";

            if (isNewPlan) {
                Integer[] ciTot = {0, 0};
                @SuppressWarnings("UnusedAssignment") Integer[] ci = null;

                //for book: get nb chapters & nb verses
                final String[] books = cols[1].split(",");
                for (String bookNumber : books) {
                    bNumber = Integer.parseInt(bookNumber);
                    ci = _s.GetBibleCiByBook(bNumber);

                    ciTot[0] += ci[0];
                    ciTot[1] += ci[1];
                }
                bCount = books.length;
                cCount = ciTot[0];
                vCount = ciTot[1];

                pd = new PlanDescBO();
                pd.planId = _s.GetPlanDescIdMax() + 1;
                pd.planRef = planRef;
                pd.bCount = bCount;
                pd.cCount = cCount;
                pd.vCount = vCount;
                pd.origVCount = vCount;

                //noinspection UnusedAssignment
                ci = null;
                //noinspection UnusedAssignment
                ciTot = null;
            } else {
                pd = _s.GetPlanDesc(planId);
                if (pd == null) return;

                bCount = pd.bCount;
                cCount = pd.cCount;
                vCount = pd.vCount;
            }

            //Dialog
            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_plan, (ViewGroup) this.findViewById(R.id.llPlan));

            final int planRefResId = PCommon.GetResId(getApplicationContext(), planRef);
            final String builderTitle = PCommon.ConcaT(getString(R.string.mnuPlan), ": ", getString(planRefResId));
            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(builderTitle);
            builder.setView(view);

            final String strPlanDesc = PCommon.ConcaT(getResources().getString(R.string.planBookCount), ": ", bCount, "\n",
                    getResources().getString(R.string.planChapterCount), ": ", cCount, "\n",
                    getResources().getString(R.string.planVerseCount), ": ", vCount, "\n");
            final TextView tvPlanDesc = view.findViewById(R.id.tvPlanDesc);
            tvPlanDesc.setText(strPlanDesc);
            tvPlanDesc.setTextSize(fontSize);
            if (typeface != null) tvPlanDesc.setTypeface(typeface);

            final Button btnDelete = view.findViewById(R.id.btnDelete);
            final Button btnGotoPlans = view.findViewById(R.id.btnGotoPlans);
            btnGotoPlans.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    builder.dismiss();
                    ShowPlans();
                }
            });

            if (isNewPlan) {
                final int defaultVdayCount = 40;
                final int maxVerses = vCount; //4000
                final int maxChapters = cCount; //1189;

                btnDelete.setVisibility(View.GONE);
                final ToggleButton btnPlanCalVerseMode = view.findViewById(R.id.btnPlanCalVerseMode);
                final ToggleButton btnPlanCalChapterMode = view.findViewById(R.id.btnPlanCalChapterMode);
                pd.planType = btnPlanCalVerseMode.isChecked() ? PlanDescBO.PLAN_TYPE.VERSE_TYPE : PlanDescBO.PLAN_TYPE.CHAPTER_TYPE;

                final TextView tvTitlePlannedMode = view.findViewById(R.id.tvTitlePlannedMode);
                final TextView tvTitleVerseCount = view.findViewById(R.id.tvTitleVerseCount);
                final TextView tvTitleDayCount = view.findViewById(R.id.tvTitleDayCount);
                final TextView tvTitleStartDate = view.findViewById(R.id.tvTitleStartDate);
                PCommon.SetTextAppareance(tvTitlePlannedMode, this, R.style.TextAppearance_AppCompat_Headline);
                PCommon.SetTextAppareance(tvTitleVerseCount, this, R.style.TextAppearance_AppCompat_Headline);
                PCommon.SetTextAppareance(tvTitleDayCount, this, R.style.TextAppearance_AppCompat_Headline);
                PCommon.SetTextAppareance(tvTitleStartDate, this, R.style.TextAppearance_AppCompat_Headline);
                if (typeface != null) {
                    tvTitlePlannedMode.setTypeface(typeface);
                    tvTitleVerseCount.setTypeface(typeface);
                    tvTitleDayCount.setTypeface(typeface);
                    tvTitleStartDate.setTypeface(typeface);
                }

                final NumberPicker npVerseChapterCount = view.findViewById(R.id.npVerseChapterCount);
                final NumberPicker npDayCount = view.findViewById(R.id.npDayCount);

                npVerseChapterCount.setMinValue(1);
                npVerseChapterCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxVerses));
                npVerseChapterCount.setValue(defaultVdayCount);
                npVerseChapterCount.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        pd.vDayCount = npVerseChapterCount.getValue();
                        pd.dayCount = Div(pd.vCount, pd.vDayCount);
                        npDayCount.setValue(pd.dayCount);
                    }
                });

                npDayCount.setMinValue(1);
                npDayCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxDays));
                npDayCount.setValue(Div(pd.vCount, defaultVdayCount));
                npDayCount.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        pd.dayCount = npDayCount.getValue();
                        pd.vDayCount = Div(pd.vCount, pd.dayCount);
                        npVerseChapterCount.setValue(pd.vDayCount);
                    }
                });

                final View glPlanCalMeasures = view.findViewById(R.id.glPlanCalMeasures);
                glPlanCalMeasures.setVisibility(View.VISIBLE);
                final View clPlanCalMode = view.findViewById(R.id.clPlanCalMode);
                clPlanCalMode.setVisibility(View.VISIBLE);

                btnPlanCalVerseMode.setChecked(true);
                btnPlanCalChapterMode.setChecked(false);
                btnPlanCalVerseMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                        if (isChecked) {
                            btnPlanCalVerseMode.setChecked(true);
                            btnPlanCalChapterMode.setChecked(false);

                            tvTitleVerseCount.setText(v.getContext().getString(R.string.planCalTitleVerseCount));
                            PCommon.SetTextAppareance(tvTitleVerseCount, v.getContext(), R.style.TextAppearance_AppCompat_Headline);
                            if (typeface != null) tvTitleVerseCount.setTypeface(typeface);

                            pd.planType = PlanDescBO.PLAN_TYPE.VERSE_TYPE;
                            pd.vCount = vCount;

                            npVerseChapterCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxVerses));
                            npVerseChapterCount.setValue(npVerseChapterCount.getValue());

                            npDayCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxDays));
                            npDayCount.setValue(Div(pd.vCount, npVerseChapterCount.getValue()));

                        } else {
                            btnPlanCalVerseMode.setChecked(false);
                            btnPlanCalChapterMode.setChecked(true);

                            tvTitleVerseCount.setText(v.getContext().getString(R.string.planCalTitleChapterCount));
                            PCommon.SetTextAppareance(tvTitleVerseCount, v.getContext(), R.style.TextAppearance_AppCompat_Headline);
                            if (typeface != null) tvTitleVerseCount.setTypeface(typeface);

                            pd.planType = PlanDescBO.PLAN_TYPE.CHAPTER_TYPE;
                            pd.vCount = cCount;

                            npVerseChapterCount.setMaxValue(maxChapters); //setMaxValue(Math.min(pd.vCount, maxChapters));
                            npVerseChapterCount.setValue(npVerseChapterCount.getValue());

                            npDayCount.setMaxValue(maxChapters); //setMaxValue(pd.vCount < maxChapters ? pd.vCount : maxDays);
                            npDayCount.setValue(Div(pd.vCount, npVerseChapterCount.getValue()));
                        }
                    }
                });
                btnPlanCalChapterMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                        if (isChecked) {
                            btnPlanCalVerseMode.setChecked(false);
                            btnPlanCalChapterMode.setChecked(true);

                            tvTitleVerseCount.setText(v.getContext().getString(R.string.planCalTitleChapterCount));
                            PCommon.SetTextAppareance(tvTitleVerseCount, v.getContext(), R.style.TextAppearance_AppCompat_Headline);
                            if (typeface != null) tvTitleVerseCount.setTypeface(typeface);

                            pd.planType = PlanDescBO.PLAN_TYPE.CHAPTER_TYPE;
                            pd.vCount = cCount;

                            npVerseChapterCount.setMaxValue(maxChapters); //setMaxValue(Math.min(pd.vCount, maxChapters));
                            npVerseChapterCount.setValue(npVerseChapterCount.getValue());

                            npDayCount.setMaxValue(maxChapters); //setMaxValue(pd.vCount < maxChapters ? pd.vCount : maxDays);
                            npDayCount.setValue(Div(pd.vCount, npVerseChapterCount.getValue()));

                        } else {
                            btnPlanCalVerseMode.setChecked(true);
                            btnPlanCalChapterMode.setChecked(false);

                            tvTitleVerseCount.setText(v.getContext().getString(R.string.planCalTitleVerseCount));
                            PCommon.SetTextAppareance(tvTitleVerseCount, v.getContext(), R.style.TextAppearance_AppCompat_Headline);
                            if (typeface != null) tvTitleVerseCount.setTypeface(typeface);

                            pd.planType = PlanDescBO.PLAN_TYPE.VERSE_TYPE;
                            pd.vCount = vCount;

                            npVerseChapterCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxVerses));
                            npVerseChapterCount.setValue(npVerseChapterCount.getValue());

                            npDayCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxDays));
                            npDayCount.setValue(Div(pd.vCount, npVerseChapterCount.getValue()));
                        }
                    }
                });

                final Calendar nowCal = Calendar.getInstance();
                pd.startDt = DateFormat.format(dtFormat, nowCal).toString();
                final Button btnPlanSetStartDt = view.findViewById(R.id.btnPlanSetStartDt);
                btnPlanSetStartDt.setText(pd.startDt);
                btnPlanSetStartDt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final DatePickerDialog.OnDateSetListener setDateListener = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                final Calendar startDt = Calendar.getInstance();
                                startDt.set(year, month, dayOfMonth);
                                final String startDtStr = DateFormat.format(dtFormat, startDt).toString();
                                btnPlanSetStartDt.setText(startDtStr);
                                pd.startDt = startDtStr;
                            }
                        };
                        final DatePickerDialog dtFragment = new DatePickerDialog(v.getContext(), setDateListener, nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH), nowCal.get(Calendar.DAY_OF_MONTH));
                        dtFragment.show();
                    }
                });

                final Button btnPlanCreate = view.findViewById(R.id.btnPlanCreate);
                btnPlanCreate.setVisibility(View.VISIBLE);
                btnPlanCreate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        btnPlanCalVerseMode.setEnabled(false);
                        btnPlanCalChapterMode.setEnabled(false);
                        btnPlanCreate.setEnabled(false);
                        btnGotoPlans.setEnabled(false);

                        pd.planType = btnPlanCalChapterMode.isChecked() ? PlanDescBO.PLAN_TYPE.CHAPTER_TYPE : PlanDescBO.PLAN_TYPE.VERSE_TYPE;
                        pd.vDayCount = npVerseChapterCount.getValue();
                        pd.dayCount = Div(pd.vCount, pd.vDayCount);

                        if (pd.planType == PlanDescBO.PLAN_TYPE.VERSE_TYPE) {
                            if (pd.dayCount > maxVerses || pd.vDayCount > maxVerses) {
                                final int msgId = pd.dayCount > maxVerses ? R.string.seeMat24_32 : R.string.seeGen2_2;
                                PCommon.ShowToast(getApplicationContext(), msgId, Toast.LENGTH_LONG);
                                builder.dismiss();
                                ShowPlans();
                                return;
                            }
                        } else {
                            if (pd.dayCount > maxChapters || pd.vDayCount > maxChapters) {
                                final int msgId = pd.dayCount > maxChapters ? R.string.seeMat24_32 : R.string.seeGen2_2;
                                PCommon.ShowToast(getApplicationContext(), msgId, Toast.LENGTH_LONG);
                                builder.dismiss();
                                ShowPlans();
                                return;
                            }
                        }

                        final Calendar cal = Calendar.getInstance();
                        cal.set(Integer.parseInt(pd.startDt.substring(0, 4)),
                                Integer.parseInt(pd.startDt.substring(4, 6)),
                                Integer.parseInt(pd.startDt.substring(6, 8)));
                        cal.add(Calendar.DAY_OF_MONTH, pd.dayCount);
                        pd.endDt = DateFormat.format(dtFormat, cal).toString();

                        final ProgressDialog pgr = new ProgressDialog(view.getContext());
                        pgr.setMessage(getString(R.string.planCreating));
                        pgr.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        pgr.setIndeterminate(true);
                        pgr.show();

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                pd.planId = _s.GetPlanDescIdMax() + 1;
                                _s.AddPlan(pd, cols[1]);

                                builder.dismiss();
                                ShowPlans();

                                pgr.dismiss();
                            }
                        }, 500);
                    }
                });
            } else {
                btnDelete.setVisibility(View.VISIBLE);
                btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ShowPlansMenu(builder, planId);
                    }
                });
                final Button btnBack = view.findViewById(R.id.btnBack);
                btnBack.setVisibility(View.VISIBLE);
                btnBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                        ShowPlan(planId, fpageNumber - 1);
                    }
                });
                final Button btnForward = view.findViewById(R.id.btnForward);
                btnForward.setVisibility(View.VISIBLE);
                btnForward.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        builder.dismiss();
                        ShowPlan(planId, fpageNumber + 1);
                    }
                });
                if (fpageNumber == 0) btnBack.setEnabled(false);
                if (fpageNumber == pageCount - 1) btnForward.setEnabled(false);

                final GridLayout glCal = view.findViewById(R.id.glCal);
                glCal.setVisibility(View.VISIBLE);

                final ArrayList<PlanCalBO> lstCal = _s.GetPlanCal(bbname, pd.planId, fpageNumber);
                TextView tvDay, tvUntil, tvTitleIsRead;
                CheckBox chkIsRead;
                String strDay, strUntil;

                tvDay = new TextView(this);
                tvDay.setLayoutParams(PCommon._layoutParamsWrap);
                tvDay.setPadding(10, 10, 10, 10);
                PCommon.SetTextAppareance(tvDay, this, R.style.TextAppearance_AppCompat_Headline);
                tvDay.setText(Html.fromHtml(PCommon.ConcaT("<b>", getString(R.string.planCalTitleDt).replaceFirst("\n", "<br><u>"), "</b>")));
                if (typeface != null) tvDay.setTypeface(typeface);

                tvUntil = new TextView(this);
                tvUntil.setLayoutParams(PCommon._layoutParamsWrap);
                tvUntil.setPadding(10, 10, 10, 10);
                PCommon.SetTextAppareance(tvUntil, this, R.style.TextAppearance_AppCompat_Headline);
                tvUntil.setText(Html.fromHtml(PCommon.ConcaT("<b>", getString(R.string.planCalTitleUntil).replaceFirst("\n", "<br><u>"), "</b>")));
                if (typeface != null) tvUntil.setTypeface(typeface);

                tvTitleIsRead = new TextView(this);
                tvTitleIsRead.setLayoutParams(PCommon._layoutParamsWrap);
                tvTitleIsRead.setPadding(10, 10, 10, 10);
                PCommon.SetTextAppareance(tvTitleIsRead, this, R.style.TextAppearance_AppCompat_Headline);
                tvTitleIsRead.setText(Html.fromHtml(PCommon.ConcaT("<b>", getString(R.string.planCalTitleIsRead).replaceFirst("\n", "<br><u>"), "</b>")));
                if (typeface != null) tvTitleIsRead.setTypeface(typeface);

                glCal.addView(tvTitleIsRead);
                glCal.addView(tvDay);
                glCal.addView(tvUntil);

                final int nowDayNumber = _s.GetCurrentDayNumberOfPlanCal(planId);
                for (PlanCalBO pc : lstCal) {
                    chkIsRead = new CheckBox(this);
                    chkIsRead.setLayoutParams(PCommon._layoutParamsWrap);
                    chkIsRead.setPadding(10, 10, 10, 10);
                    chkIsRead.setEnabled(true);
                    chkIsRead.setTag(R.id.tv1, pc.planId);
                    chkIsRead.setTag(R.id.tv2, pc.dayNumber);
                    chkIsRead.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                            final int planId = (int) v.getTag(R.id.tv1);
                            final int dayNumber = (int) v.getTag(R.id.tv2);
                            final int isRead = isChecked ? 1 : 0;
                            _s.MarkPlanCal(planId, dayNumber, isRead);
                        }
                    });
                    chkIsRead.setChecked(pc.isRead == 1);

                    tvDay = new TextView(this);
                    tvDay.setLayoutParams(PCommon._layoutParamsWrap);
                    tvDay.setPadding(10, 10, 10, 10);
                    strDay = PCommon.ConcaT(pc.dayNumber, "\n", pc.dayDt);
                    tvDay.setText(strDay);
                    tvDay.setTextSize(fontSize);
                    if (typeface != null) {
                        if (pc.dayNumber == nowDayNumber) {
                            tvDay.setTypeface(typeface, Typeface.BOLD);
                        } else {
                            tvDay.setTypeface(typeface);
                        }
                    }
                    tvDay.setTag(R.id.tv1, pc.planId);
                    tvDay.setTag(R.id.tv2, pc.dayNumber);
                    tvDay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final int planId = (int) v.getTag(R.id.tv1);
                            final int dayNumber = (int) v.getTag(R.id.tv2);
                            ShowPlanMenu(builder, planId, dayNumber, fpageNumber);
                        }
                    });
                    tvDay.setFocusable(true);
                    tvDay.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                    tvUntil = new TextView(this);
                    tvUntil.setLayoutParams(PCommon._layoutParamsWrap);
                    tvUntil.setPadding(10, 10, 10, 10);
                    strUntil = PCommon.ConcaT(pc.bsNameStart, " ", pc.cNumberStart, ".", pc.vNumberStart, "\n", pc.bsNameEnd, " ", pc.cNumberEnd, ".", pc.vNumberEnd);
                    tvUntil.setText(strUntil);
                    if (typeface != null) tvUntil.setTypeface(typeface);
                    tvUntil.setTextSize(fontSize);
                    tvUntil.setTag(R.id.tv1, pc.planId);
                    tvUntil.setTag(R.id.tv2, pc.dayNumber);
                    tvUntil.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final int planId = (int) v.getTag(R.id.tv1);
                            final int dayNumber = (int) v.getTag(R.id.tv2);
                            ShowPlanMenu(builder, planId, dayNumber, fpageNumber);
                        }
                    });
                    tvUntil.setFocusable(true);
                    tvUntil.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                    glCal.addView(chkIsRead);
                    glCal.addView(tvDay);
                    glCal.addView(tvUntil);
                }
                btnGotoPlans.requestFocus();
            }

            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Show plan context menu
     * @param dlgPlan       Parent dialog
     * @param planId        Plan Id
     * @param dayNumber     Day number
     * @param pageNumber    Page number
     */
    private void ShowPlanMenu(final AlertDialog dlgPlan, final int planId, final int dayNumber, final int pageNumber) {
        try {
            CheckLocalInstance(this);

            final PlanDescBO pd = _s.GetPlanDesc(planId);
            if (pd == null) return;

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_plan_menu, (ViewGroup) this.findViewById(R.id.llPlanMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuPlanReading);
            builder.setView(view);

            final int resId = PCommon.GetResId(getApplicationContext(), pd.planRef);
            final String planTitle = PCommon.ConcaT("<b>", getString(resId), ":</b>");
            final TextView tvPlanTitle = view.findViewById(R.id.tvToolsTitle);
            tvPlanTitle.setText(Html.fromHtml(planTitle));
            if (typeface != null) tvPlanTitle.setTypeface(typeface);
            tvPlanTitle.setTextSize(fontSize);

            final Button btnOpen = view.findViewById(R.id.btnOpen);
            btnOpen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            final String bbname = PCommon.GetPrefBibleName(getApplicationContext());
                            final String fullQuery = PCommon.ConcaT(planId, " ", dayNumber, " ", pageNumber);

                            final AlertDialog builderLanguages = new AlertDialog.Builder(view.getContext()).create();             //, R.style.DialogStyleKaki
                            final LayoutInflater inflater = getLayoutInflater();
                            final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, (ViewGroup) findViewById(R.id.llLanguages));
                            final String msg = getString(R.string.mnuPlanReading);
                            PCommon.SelectBibleLanguageMulti(builderLanguages, view.getContext(), vllLanguages, msg, "", true, false);
                            builderLanguages.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    final String bbnamed = PCommon.GetPref(view.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbname);
                                    if (bbnamed.equals("")) return;
                                    final String tbbName = PCommon.GetPrefTradBibleName(view.getContext(), true);
                                    MainActivity.Tab.AddTab(getApplicationContext(), "P", tbbName, fullQuery);
                                    builder.dismiss();
                                    dlgPlan.dismiss();
                                }
                            });
                            builderLanguages.show();
                        }
                    });
                }
            });
            final Button btnMarkAllAboveAsRead = view.findViewById(R.id.btnMarkAllAboveAsRead);
            btnMarkAllAboveAsRead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            _s.MarkAllAbovePlanCal(planId, dayNumber, 1);
                            builder.dismiss();
                            dlgPlan.dismiss();
                            ShowPlan(planId, pageNumber);
                        }
                    });
                }
            });
            final Button btnUnmarkAllAboveAsRead = view.findViewById(R.id.btnUnmarkAllAboveAsRead);
            btnUnmarkAllAboveAsRead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            _s.MarkAllAbovePlanCal(planId, dayNumber, 0);
                            builder.dismiss();
                            dlgPlan.dismiss();
                            ShowPlan(planId, pageNumber);
                        }
                    });
                }
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowTodos() {
        try {
            CheckLocalInstance(this);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            //Dialog
            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_todos, (ViewGroup) this.findViewById(R.id.llTodos));
            final Button btnTdAdd = view.findViewById(R.id.btnTdAdd);
            final Button btnTdResetStatus = view.findViewById(R.id.btnTdResetStatus);
            final ToggleButton btnTdTodoStatusType = view.findViewById(R.id.btnTdTodoStatusType);
            final ToggleButton btnTdDoneStatusType = view.findViewById(R.id.btnTdDoneStatusType);
            final ToggleButton btnTdWaitStatusType = view.findViewById(R.id.btnTdWaitStatusType);
            final GridLayout glTd = view.findViewById(R.id.glTodos);

            final String builderTitle = getString(R.string.mnuTodos);
            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(builderTitle);
            builder.setView(view);

            final String tdTitleTodoStatusType = PCommon.ConcaT(getString(R.string.tdTitleTodoStatusType), "\n(", _s.GetTodoCountByStatus("TODO"), ")");
            btnTdTodoStatusType.setTextOn(tdTitleTodoStatusType);
            btnTdTodoStatusType.setTextOff(tdTitleTodoStatusType);
            final String tdTitleDoneStatusType = PCommon.ConcaT(getString(R.string.tdTitleDoneStatusType), "\n(", _s.GetTodoCountByStatus("DONE"), ")");
            btnTdDoneStatusType.setTextOn(tdTitleDoneStatusType);
            btnTdDoneStatusType.setTextOff(tdTitleDoneStatusType);
            final String tdTitleWaitStatusType = PCommon.ConcaT(getString(R.string.tdTitleWaitStatusType), "\n(", _s.GetTodoCountByStatus("WAIT"), ")");
            btnTdWaitStatusType.setTextOn(tdTitleWaitStatusType);
            btnTdWaitStatusType.setTextOff(tdTitleWaitStatusType);

            btnTdTodoStatusType.setChecked(false);
            btnTdDoneStatusType.setChecked(false);
            btnTdWaitStatusType.setChecked(false);

            final String todoType = PCommon.GetPref(this, IProject.APP_PREF_KEY.TODO_STATUS, "TODO");
            if (todoType.compareTo("DONE") == 0) {
                btnTdDoneStatusType.setChecked(true);
            } else if (todoType.compareTo("WAIT") == 0) {
                btnTdWaitStatusType.setChecked(true);
            } else {
                btnTdTodoStatusType.setChecked(true);
            }

            final ArrayList<TodoBO> lstTd = _s.GetListTodoByStatus(todoType);
            for (final TodoBO td : lstTd) {
                if (td != null) {
                    final String tdPriority = td.tdPriority.compareTo("HIGH") == 0 ? getString(R.string.priorityHigh) : td.tdPriority.compareTo("LOW") == 0 ? getString(R.string.priorityLow) : getString(R.string.bulletDefault);
                    final String tdDesc = PCommon.ConcaT(Html.fromHtml(PCommon.ConcaT(tdPriority, "&nbsp;")), td.tdDesc);
                    final TextView tvDesc = new TextView(this);
                    tvDesc.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                    tvDesc.setPadding(20, 20, 20, 20);
                    tvDesc.setText(tdDesc);
                    tvDesc.setTextSize(fontSize);
                    if (typeface != null) {
                        tvDesc.setTypeface(typeface);
                    }
                    tvDesc.setTag(R.id.tv1, td.tdId);
                    tvDesc.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    builder.dismiss();
                                    final int tdId = (int) v.getTag(R.id.tv1);
                                    ShowTodo(false, tdId);
                                }
                            }, 0);
                        }
                    });
                    tvDesc.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(final View v) {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    final int tdId = (int) v.getTag(R.id.tv1);
                                    ShowTodoMenu(builder, 0, tdId);
                                }
                            }, 0);
                            return false;
                        }
                    });
                    tvDesc.setFocusable(true);
                    tvDesc.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));
                    glTd.addView(tvDesc);
                }
            }
            btnTdAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "TODO");

                            builder.dismiss();
                            ShowTodo(true, -1);
                        }
                    }, 0);
                }
            });
            btnTdResetStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ShowTodoMenu(builder, 1, -1);
                        }
                    }, 0);
                }
            });
            btnTdTodoStatusType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "TODO");
                            btnTdTodoStatusType.setChecked(true);
                            btnTdDoneStatusType.setChecked(false);
                            btnTdWaitStatusType.setChecked(false);
                            builder.dismiss();
                            ShowTodos();
                        }
                    }, 0);
                }
            });
            btnTdDoneStatusType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "DONE");
                            btnTdTodoStatusType.setChecked(false);
                            btnTdDoneStatusType.setChecked(true);
                            btnTdWaitStatusType.setChecked(false);
                            builder.dismiss();
                            ShowTodos();
                        }
                    }, 0);
                }
            });
            btnTdWaitStatusType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "WAIT");
                            btnTdTodoStatusType.setChecked(false);
                            btnTdDoneStatusType.setChecked(false);
                            btnTdWaitStatusType.setChecked(true);
                            builder.dismiss();
                            ShowTodos();
                        }
                    }, 0);
                }
            });
            builder.show();

            //--
            final int todoIdCount = _s.GetTodoIdCount();
            if (todoIdCount <= 0) {
                if (!isTodoSelectAlreadyWarned) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isTodoSelectAlreadyWarned = true;
                            PCommon.ShowDialog(MainActivity.this, R.string.tdSelect, false, R.string.tdSelectMsg);
                        }
                    }, 0);
                }
            }
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowTodo(final boolean isNewTodo, final int tdId) {
        try {
            CheckLocalInstance(this);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            //Dialog
            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_todo, (ViewGroup) this.findViewById(R.id.llTodo));

            final String builderTitle = getString(R.string.mnuTodos);
            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(builderTitle);
            builder.setView(view);
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    ShowTodos();
                }
            });

            final TextView tvTdTitleDesc = view.findViewById(R.id.tvTdTitleDesc);
            final TextView tvTdTitleCommentIssues = view.findViewById(R.id.tvTdTitleCommentIssues);
            final TextView tvTdTitlePriorityType = view.findViewById(R.id.tvTdTitlePriorityType);
            final TextView tvTdTitleStatusType = view.findViewById(R.id.tvTdTitleStatusType);

            final EditText etTdDesc = view.findViewById(R.id.etTdDesc);
            final EditText etTdCommentIssues = view.findViewById(R.id.etTdCommentIssues);

            PCommon.SetTextAppareance(tvTdTitleDesc, this, R.style.TextAppearance_AppCompat_Headline);
            PCommon.SetTextAppareance(tvTdTitleCommentIssues, this, R.style.TextAppearance_AppCompat_Headline);
            PCommon.SetTextAppareance(tvTdTitlePriorityType, this, R.style.TextAppearance_AppCompat_Headline);
            PCommon.SetTextAppareance(tvTdTitleStatusType, this, R.style.TextAppearance_AppCompat_Headline);

            if (typeface != null) {
                tvTdTitleDesc.setTypeface(typeface);
                tvTdTitleCommentIssues.setTypeface(typeface);
                tvTdTitlePriorityType.setTypeface(typeface);
                tvTdTitleStatusType.setTypeface(typeface);

                etTdDesc.setTypeface(typeface);
                etTdCommentIssues.setTypeface(typeface);
            }

            etTdDesc.setTextSize(fontSize);
            etTdCommentIssues.setTextSize(fontSize);

            final ToggleButton btnTdTodoStatusType = view.findViewById(R.id.btnTdTodoStatusType);
            final ToggleButton btnTdDoneStatusType = view.findViewById(R.id.btnTdDoneStatusType);
            final ToggleButton btnTdWaitStatusType = view.findViewById(R.id.btnTdWaitStatusType);
            final ToggleButton btnTdNormalPriorityType = view.findViewById(R.id.btnTdNormalPriorityType);
            final ToggleButton btnTdHighPriorityType = view.findViewById(R.id.btnTdHighPriorityType);
            final ToggleButton btnTdLowPriorityType = view.findViewById(R.id.btnTdLowPriorityType);

            final Button btnTdDelete = view.findViewById(R.id.btnTdDelete);
            final Button btnTdSave = view.findViewById(R.id.btnTdSave);

            btnTdTodoStatusType.setChecked(false);
            btnTdDoneStatusType.setChecked(false);
            btnTdWaitStatusType.setChecked(false);

            btnTdNormalPriorityType.setChecked(false);
            btnTdHighPriorityType.setChecked(false);
            btnTdLowPriorityType.setChecked(false);

            btnTdDelete.setVisibility(View.GONE);
            btnTdTodoStatusType.requestFocus();

            if (isNewTodo) {
                btnTdTodoStatusType.setChecked(true);
                btnTdNormalPriorityType.setChecked(true);

                etTdDesc.setText(getString(R.string.textEmpty));
                etTdCommentIssues.setText(getString(R.string.textEmpty));
            } else {
                final TodoBO td = _s.GetTodo(tdId);
                if (td != null) {
                    btnTdDelete.setVisibility(View.VISIBLE);

                    etTdDesc.setText(td.tdDesc);
                    etTdCommentIssues.setText(td.tdCommentIssues);

                    if (td.tdStatus.compareTo("DONE") == 0) {
                        btnTdDoneStatusType.setChecked(true);
                    } else if (td.tdStatus.compareTo("WAIT") == 0) {
                        btnTdWaitStatusType.setChecked(true);
                    } else {
                        btnTdTodoStatusType.setChecked(true);
                    }

                    if (td.tdPriority.compareTo("HIGH") == 0) {
                        btnTdHighPriorityType.setChecked(true);
                    } else if (td.tdPriority.compareTo("LOW") == 0) {
                        btnTdLowPriorityType.setChecked(true);
                    } else {
                        btnTdNormalPriorityType.setChecked(true);
                    }
                }
            }

            btnTdTodoStatusType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnTdTodoStatusType.setChecked(true);
                    btnTdDoneStatusType.setChecked(false);
                    btnTdWaitStatusType.setChecked(false);
                }
            });
            btnTdDoneStatusType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnTdTodoStatusType.setChecked(false);
                    btnTdDoneStatusType.setChecked(true);
                    btnTdWaitStatusType.setChecked(false);
                }
            });
            btnTdWaitStatusType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnTdTodoStatusType.setChecked(false);
                    btnTdDoneStatusType.setChecked(false);
                    btnTdWaitStatusType.setChecked(true);
                }
            });

            btnTdNormalPriorityType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnTdNormalPriorityType.setChecked(true);
                    btnTdHighPriorityType.setChecked(false);
                    btnTdLowPriorityType.setChecked(false);
                }
            });
            btnTdHighPriorityType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnTdNormalPriorityType.setChecked(false);
                    btnTdHighPriorityType.setChecked(true);
                    btnTdLowPriorityType.setChecked(false);
                }
            });
            btnTdLowPriorityType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnTdNormalPriorityType.setChecked(false);
                    btnTdHighPriorityType.setChecked(false);
                    btnTdLowPriorityType.setChecked(true);
                }
            });
            btnTdDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ShowTodoMenu(builder, 0, tdId);
                        }
                    }, 0);
                }
            });
            btnTdSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    try {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                etTdDesc.setText(etTdDesc.getText().toString().trim().replaceAll("\n", ""));
                                etTdCommentIssues.setText(etTdCommentIssues.getText().toString().trim().replaceAll("\n", ""));

                                if (etTdDesc.length() == 0) {
                                    etTdDesc.requestFocus();
                                    return;
                                }

                                final TodoBO td = new TodoBO();
                                td.tdId = isNewTodo ? _s.GetNewTodoId() : tdId;
                                td.tdStatus = btnTdDoneStatusType.isChecked() ? "DONE" : btnTdWaitStatusType.isChecked() ? "WAIT" : "TODO";
                                td.tdPriority = btnTdHighPriorityType.isChecked() ? "HIGH" : btnTdLowPriorityType.isChecked() ? "LOW" : "NORMAL";
                                td.tdDesc = etTdDesc.getText().toString();
                                td.tdCommentIssues = etTdCommentIssues.getText().toString();

                                if (isNewTodo) {
                                    _s.AddTodo(td);
                                } else {
                                    _s.UpdateTodo(td);
                                }

                                builder.dismiss();
                                ShowTodos();
                            }
                        }, 0);
                    } catch (Exception ex) {
                        if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
                    }
                }
            });

            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Show Todo context menu
     * @param dlgTodo   Parent dialog
     * @param action    0 = Delete, 1 = Reset
     * @param tdId      Todo Id
     */
    private void ShowTodoMenu(final AlertDialog dlgTodo, final int action, final int tdId) {
        try {
            CheckLocalInstance(this);

            if (action == 0) {
                final TodoBO td = _s.GetTodo(tdId);
                if (td == null) return;
            }

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_todo_menu, (ViewGroup) this.findViewById(R.id.llTodoMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuTodos);
            builder.setView(view);

            final String tdTitleMenu = PCommon.ConcaT("<b>", getString(action == 0 ? R.string.tdTask : R.string.tdReset), ":</b>");
            final TextView tvTdTitleMenu = view.findViewById(R.id.tvTdTitleMenu);
            tvTdTitleMenu.setText(Html.fromHtml(tdTitleMenu));
            tvTdTitleMenu.setTextSize(fontSize);
            if (typeface != null) tvTdTitleMenu.setTypeface(typeface);

            final TextView tvTdMsgMenu = view.findViewById(R.id.tvTdMsgMenu);
            tvTdMsgMenu.setVisibility(View.GONE);
            if (action == 1) {
                final String tdMsgMenu = getString(R.string.tdResetMsg);
                tvTdMsgMenu.setVisibility(View.VISIBLE);
                tvTdMsgMenu.setText(Html.fromHtml(tdMsgMenu));
                tvTdMsgMenu.setTextSize(fontSize);
                if (typeface != null) tvTdMsgMenu.setTypeface(typeface);
                tvTdMsgMenu.setFocusable(true);
                tvTdMsgMenu.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));
            }

            final Button btnTdActionMenu = view.findViewById(R.id.btnTdActionMenu);
            btnTdActionMenu.setText(action == 0 ? R.string.tdDelete : R.string.tdReset);
            btnTdActionMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    btnTdActionMenu.setEnabled(false);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (action == 0) {
                                _s.DeleteTodo(tdId);
                            } else if (action == 1) {
                                PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "TODO");
                                _s.ResetTodoStatus();
                            }
                            builder.dismiss();
                            dlgTodo.dismiss();
                            ShowTodos();
                        }
                    }, 0);
                }
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowTools(final Context context) {
        try {
            CheckLocalInstance(context);

            if (tabLayout == null) return;

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_tools, (ViewGroup) this.findViewById(R.id.llToolsMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuTools);
            builder.setView(view);

            final int tabCount = tabLayout.getTabCount();
            final String resToolCloseTitle = PCommon.ConcaT("<u><b>", getString(R.string.toolCloseTabsTitle), "</b> (", tabCount, ")</u>");
            final TextView tvToolCloseTitle = view.findViewById(R.id.tvToolCloseTabsTitle);
            tvToolCloseTitle.setText(Html.fromHtml(resToolCloseTitle));
            if (typeface != null) tvToolCloseTitle.setTypeface(typeface);
            tvToolCloseTitle.setTextSize(fontSize);

            final Button btnToolOpenTabs = view.findViewById(R.id.btnToolOpenTabsTest);
            final Button btnToolCloseTabs = view.findViewById(R.id.btnToolCloseTabs);

            btnToolOpenTabs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnToolOpenTabs.setEnabled(false);
                    btnToolCloseTabs.setEnabled(false);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                for (int bnumber = 1; bnumber <= 10; bnumber++) {
                                    for (int cnumber = 1; cnumber <= 15; cnumber++) {
                                        final String fullQuery = PCommon.ConcaT(bnumber, " ", cnumber);
                                        Tab.AddTab(context, "k", bnumber, cnumber, 1, fullQuery);
                                    }
                                }

                                builder.dismiss();
                                PCommon.TryQuitApplication(context);
                            } catch (Exception ex) {
                                if (PCommon._isDebugVersion)
                                    PCommon.LogR(getApplicationContext(), ex);
                            }
                        }
                    }, 500);
                }
            });
            btnToolCloseTabs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnToolOpenTabs.setEnabled(false);
                    btnToolCloseTabs.setEnabled(false);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            builder.dismiss();
                            ShowToolCloseTabs(context);
                        }
                    }, 500);
                }
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowToolCloseTabs(final Context context) {
        try {
            CheckLocalInstance(this);

            if (tabLayout == null) return;

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_tools_close_menu, (ViewGroup) this.findViewById(R.id.llToolsMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuTools);
            builder.setView(view);

            final int tabCount = tabLayout.getTabCount();
            final TextView tvToolTitle = view.findViewById(R.id.tvToolsTitle);
            tvToolTitle.setTextSize(fontSize);
            if (typeface != null) tvToolTitle.setTypeface(typeface);
            final Button btnToolCloseTabs = view.findViewById(R.id.btnToolCloseTabs);
            btnToolCloseTabs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnToolCloseTabs.setEnabled(false);

                    final ProgressDialog pgr = new ProgressDialog(view.getContext());
                    pgr.setMessage(getString(R.string.toolCloseTabsClosing));
                    pgr.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    pgr.setIndeterminate(true);
                    pgr.show();

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                CheckLocalInstance(context);

                                if (tabCount <= 1) {
                                    builder.dismiss();
                                    pgr.dismiss();
                                    return;
                                }

                                CacheTabBO cacheTab;
                                for (int tabId = tabCount - 1; tabId >= 0; tabId--) {

                                    if (tabId == 0) {
                                        final int retabCount = tabLayout.getTabCount();
                                        if (retabCount <= 1) {
                                            continue;
                                        }
                                    }

                                    cacheTab = _s.GetCacheTab(tabId);
                                    if (cacheTab != null && cacheTab.tabType.equalsIgnoreCase("P")) {
                                        continue;
                                    } else {
                                        Tab.RemoveTabAt(context, tabId);
                                    }
                                }

                                PCommon.SavePref(context, IProject.APP_PREF_KEY.TAB_SELECTED, "0");
                                Tab.SelectTabByTabId(0);

                                builder.dismiss();
                                pgr.dismiss();
                            } catch (Exception ex) {
                                if (PCommon._isDebugVersion)
                                    PCommon.LogR(getApplicationContext(), ex);
                            }
                        }
                    }, 500);
                }
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowAbout(final Context context) {
        try {
            final AlertDialog builder = new AlertDialog.Builder(context).create();                  //R.style.DialogStyleKaki
            builder.setTitle(R.string.mnuAbout);
            builder.setCancelable(true);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            sv.setSmoothScrollingEnabled(false);
            sv.setPadding(20, 20, 20, 20);

            final LinearLayout llSv = new LinearLayout(context);
            llSv.setOrientation(LinearLayout.VERTICAL);
            llSv.setLayoutParams(PCommon._layoutParamsMatch);
            llSv.setPadding(10, 10, 10, 10);
            llSv.setVerticalGravity(Gravity.CENTER_VERTICAL);
            llSv.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
            sv.addView(llSv);

            final PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            final int dbVersion = _s.GetDbVersion();
            final String app = PCommon.ConcaT("Bible Multi\n", getString(R.string.appName));
            final String devEmail = context.getString(R.string.devEmail).replaceAll("r", "");
            final String aboutDev = PCommon.ConcaT(app, "\n", pi.versionName, " (", dbVersion, ") ", pi.versionCode, "\n@", devEmail.split("@")[0], "\n");
            final String aboutContent = PCommon.ConcaT(context.getString(R.string.aboutContactMe));

            //---
            final ImageView iv = new ImageView(context);
            iv.setMaxWidth(150);
            iv.setMaxHeight(150);
            iv.setImageResource(R.drawable.thelightlogo);
            iv.setAdjustViewBounds(true);
            llSv.addView(iv);

            //---
            final TextView tvDev = new TextView(context);
            tvDev.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvDev.setPadding(0, 5, 0, 0);
            tvDev.setText(aboutDev);
            tvDev.setGravity(Gravity.CENTER_HORIZONTAL);
            tvDev.setCursorVisible(true);
            if (typeface != null) tvDev.setTypeface(typeface);
            tvDev.setTextSize(fontSize);
            tvDev.setFocusable(false);
            llSv.addView(tvDev);

            //---
            final TextView tvContent = new TextView(context);
            tvContent.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvContent.setPadding(0, 5, 0, 5);
            tvContent.setText(aboutContent);
            tvContent.setGravity(Gravity.CENTER_HORIZONTAL);
            tvContent.setCursorVisible(true);
            if (typeface != null) tvContent.setTypeface(typeface);
            tvContent.setTextSize(fontSize);
            tvContent.setFocusable(true);
            tvContent.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
            llSv.addView(tvContent);

            //---
            final TextView tv1 = new TextView(context);
            tv1.setFocusable(false);
            llSv.addView(tv1);

            //---
            final Button btnEmail = new Button(context);
            btnEmail.setLayoutParams(PCommon._layoutParamsWrap);
            btnEmail.setText(R.string.sendEmail);
            btnEmail.setOnClickListener(new View.OnClickListener() {

                public void onClick(View arg0) {
                    PCommon.SendEmail(context,
                            new String[]{devEmail},
                            app,
                            "");
                }
            });
            btnEmail.setFocusable(true);
            btnEmail.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnEmail);

            //---
            final TextView tv4 = new TextView(context);
            tv4.setFocusable(false);
            llSv.addView(tv4);

            //---
            final Button btnGitlab = new Button(context);
            btnGitlab.setLayoutParams(PCommon._layoutParamsWrap);
            btnGitlab.setText(R.string.btnGitlab);
            btnGitlab.setOnClickListener(new View.OnClickListener() {

                public void onClick(View vw) {
                    PCommon.OpenUrl(vw.getContext(), "XhXttXpXsX:X/X/XXgXitXlabX.XcoXXmX/XhXotlXitXtlewXhitXXedoXgX/XBXibXlXeXXXMXultiXThXeLXighXtX/XiXssXueXs".replaceAll("X", ""));
                }
            });
            btnGitlab.setFocusable(true);
            btnGitlab.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnGitlab);

            //---
            final TextView tv2 = new TextView(context);
            tv2.setFocusable(false);
            llSv.addView(tv2);

            //---
            final Button btnXda = new Button(context);
            btnXda.setLayoutParams(PCommon._layoutParamsWrap);
            btnXda.setText(R.string.btnXda);
            btnXda.setOnClickListener(new View.OnClickListener() {

                public void onClick(View vw) {
                    PCommon.OpenUrl(vw.getContext(), "ZhZtZtZpZsZ:Z/Z/fZorZuZmZ.ZxZdZa-ZdZeZvZZZZeZloZpZerZsZ.ZcZoZmZ/ZtZ/ZapZpZ-Z5-0-ZbiZbZlZe-ZmuZlZtZi-tZhZe-lZiZgZhZtZ-oZpZen-ZsouZrcZe.Z4033Z799".replace("Z", ""));
                }
            });
            btnXda.setFocusable(true);
            btnXda.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnXda);

            //---
            final TextView tv5 = new TextView(context);
            tv5.setFocusable(false);
            llSv.addView(tv5);

            //---
            final Button btnFb = new Button(context);
            btnFb.setLayoutParams(PCommon._layoutParamsWrap);
            btnFb.setText(R.string.btnFb);
            btnFb.setOnClickListener(new View.OnClickListener() {

                public void onClick(View vw) {
                    PCommon.OpenUrl(vw.getContext(), "XhXtXtXpXsXX:X/XXX/wXwwXX.fXaXXXcXeXXXboXok.XXcXoXXXmX/XBXiXblXXeXMXXuXlXXtiXTXXheXXLXXiXgXhtX".replaceAll("X", ""));
                }
            });
            btnFb.setFocusable(true);
            btnFb.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnFb);

            //---
            final TextView tv6 = new TextView(context);
            tv6.setFocusable(false);
            llSv.addView(tv6);

            builder.setView(sv);
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void InviteFriend() {
        try {
            final String text1 = PCommon.ConcaT(getString(R.string.inviteFriendClipboardMsg1), getString(R.string.appUrls1), getString(R.string.inviteFriendClipboardMsg2));
            final String text2 = PCommon.ConcaT(getString(R.string.inviteFriendClipboardMsg1), getString(R.string.appUrls2), getString(R.string.inviteFriendClipboardMsg2));
            PCommon.CopyTextToClipboard(getApplicationContext(), "", PCommon.ConcaT(text1, "\n\n---\n\n", text2), true);
            PCommon.ShowDialog(MainActivity.this, R.string.mnuInvite, true, R.string.inviteFriendDlgMsg);
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    static class Tab {
        @SuppressLint("StaticFieldLeak")
        static SCommon _s = null;
        private static boolean isThemeWhite;

        static Boolean IsThemeWhite() {
            return isThemeWhite;
        }

        static void SetCurrentTabTitle(final String title) {
            //noinspection EmptyCatchBlock
            try {
                final int tabId = GetCurrentTabPosition();
                if (tabId < 0)
                    return;

                //noinspection ConstantConditions
                tabLayout.getTabAt(tabId).setText(title);
                PCommon.SavePref(tabLayout.getContext(), IProject.APP_PREF_KEY.TAB_SELECTED, Integer.toString(tabId));
            } catch (Exception ex) {
            }
        }

        static int GetCurrentTabPosition() {
            if (tabLayout == null)
                return -1;

            final int tabSelected = tabLayout.getSelectedTabPosition();
            if (tabSelected < 0)
                return -1;

            return tabSelected;
        }

        static void SelectTabByTabId(final int tabId) {
            if (tabId >= 0) {
                tabLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        //noinspection EmptyCatchBlock
                        try {
                            //noinspection ConstantConditions
                            tabLayout.getTabAt(tabId).select();
                        } catch (Exception ex) {
                        }
                    }
                });
            }
        }

        static int GetTabCount() {
            if (tabLayout == null)
                return -1;

            return tabLayout.getTabCount();
        }

        /***
         * Add tab empty SEARCH_TYPE, FAV_TYPE
         * @param context
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.tabTitleDefault);
                tabLayout.addTab(tab);
                FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);

                PCommon.ShowToast(context, R.string.toastMakeSearch, Toast.LENGTH_SHORT);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        /***
         * Add tab for Open chapter SEARCH_TYPE
         * @param context
         * @param tbbName
         * @param bNumber
         * @param cNumber
         * @param fullQuery
         * @param vNumber
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context, final String tbbName, final int bNumber, final int cNumber, final String fullQuery, final int vNumber) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int tabNumber = tabLayout.getTabCount();
                final String bbname = tbbName.substring(0, 1);
                final int pos = (vNumber - 1) * tbbName.length();
                final CacheTabBO t = new CacheTabBO(tabNumber, "S", context.getString(R.string.tabTitleDefault), fullQuery, pos, bbname, true, true, false, bNumber, cNumber, 0, tbbName);
                _s.SaveCacheTab(t);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.tabTitleDefault);
                tabLayout.addTab(tab);
                FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        /***
         * Add tab for Open Cross references
         * @param context
         * @param tbbName
         * @param bNumber
         * @param cNumber
         * @param fullQuery
         * @param vNumber
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context, final String tbbName, final int bNumber, final int cNumber, final int vNumber, final String fullQuery) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int tabNumber = tabLayout.getTabCount();
                final String bbname = tbbName.substring(0, 1);
                final int pos = 0;
                final CacheTabBO t = new CacheTabBO(tabNumber, "S", context.getString(R.string.tabTitleDefault), fullQuery, pos, bbname, true, false, false, bNumber, cNumber, vNumber, tbbName);
                _s.SaveCacheTab(t);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.tabTitleDefault);
                tabLayout.addTab(tab);
                FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        /***
         * Add tab for Open result PRBL | ARTICLE | PLAN | INTENT
         * @param context
         * @param cacheTabType
         * @param tbbName
         * @param fullQuery bNumber, cNumber, vNumberFrom, vNumberTo
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context, final String cacheTabType, final String tbbName, final String fullQuery) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final String bbname = tbbName.substring(0, 1);
                final int tabNumber = tabLayout.getTabCount();
                final String tabTitle;
                final CacheTabBO t;
                if (cacheTabType.equalsIgnoreCase("A")) {
                    final String resString;
                    final int tabNameSize = Integer.parseInt(context.getString(R.string.tabSizeName));

                    if (fullQuery.startsWith("ART")) {
                        final int resId = PCommon.GetResId(context, fullQuery);
                        resString = context.getString(resId);
                    } else {
                        final String myArtPrefix = context.getString(R.string.tabMyArtPrefix);
                        resString = PCommon.ConcaT(myArtPrefix, fullQuery);
                    }

                    tabTitle = (resString.length() <= tabNameSize) ? resString : fullQuery;
                    t = new CacheTabBO(tabNumber, cacheTabType, tabTitle, fullQuery, 0, bbname, true, false, false, 0, 0, 0, tbbName);
                    _s.SaveCacheTab(t);
                } else if (cacheTabType.equalsIgnoreCase("P")) {
                    final String[] cols = fullQuery.split("\\s");
                    if (cols.length != 3) return;
                    final int planId = Integer.parseInt(cols[0]);
                    final PlanDescBO pd = _s.GetPlanDesc(planId);
                    if (pd == null) return;
                    final int resId = PCommon.GetResId(context, pd.planRef);
                    tabTitle = context.getString(resId);
                    t = new CacheTabBO(tabNumber, cacheTabType, tabTitle, fullQuery, 0, bbname, true, false, false, 0, 0, 0, tbbName);
                    _s.SaveCacheTab(t);

                    final int planDayNumber = Integer.parseInt(cols[1]);
                    final PlanCalBO pc = _s.GetPlanCalByDay(bbname, planId, planDayNumber);
                    final int bNumberStart = pc.bNumberStart, cNumberStart = pc.cNumberStart, vNumberStart = pc.vNumberStart;
                    final int bNumberEnd = pc.bNumberEnd, cNumberEnd = pc.cNumberEnd, vNumberEnd = pc.vNumberEnd;
                    final int tabIdTo = MainActivity.Tab.GetTabCount();
                    final boolean copy = _s.CopyCacheSearchForOtherBible(tabIdTo, tbbName, bNumberStart, cNumberStart, vNumberStart, bNumberEnd, cNumberEnd, vNumberEnd);
                    if (!copy) return;
                } else {
                    tabTitle = context.getString(R.string.tabTitleDefault);
                    t = new CacheTabBO(tabNumber, cacheTabType, tabTitle, fullQuery, 0, bbname, true, false, false, 0, 0, 0, tbbName);
                    _s.SaveCacheTab(t);
                }
                final TabLayout.Tab tab = tabLayout.newTab().setText(tabTitle);
                tabLayout.addTab(tab);
                FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        /***
         * Add tab for Open result SEARCH_TYPE
         * @param context
         * @param tabNumberFrom
         * @param bbNameTo
         * @param vNumber
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context, final int tabNumberFrom, final String bbNameTo, final int vNumber) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int pos = (vNumber - 1) * bbNameTo.length();
                final int tabNumberTo = tabLayout.getTabCount();
                final CacheTabBO t = _s.GetCacheTab(tabNumberFrom);
                t.tabType = "S";
                t.tabNumber = tabNumberTo;
                t.bbName = bbNameTo.substring(0, 1);
                t.scrollPosY = pos;
                t.isBook = true;
                t.isChapter = false;
                t.isVerse = false;
                t.trad = bbNameTo;
                _s.SaveCacheTab(t);
                _s.CopyCacheSearchForOtherBible(tabNumberFrom, tabNumberTo, bbNameTo);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.tabTitleDefault);
                tabLayout.addTab(tab);
                FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        static void RemoveCurrentTab(final Context context) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int tabNumberToRemove = tabLayout.getSelectedTabPosition();

                Tab.RemoveTabAt(context, tabNumberToRemove);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        static void RemoveTabAt(final Context context, final int tabNumberToRemove) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int tabCount = tabLayout.getTabCount();
                if (tabCount <= 1)
                    return;

                _s.DeleteCache(tabNumberToRemove);

                int toTabId;
                for (int fromTabId = tabNumberToRemove + 1; fromTabId < tabCount; fromTabId++) {
                    toTabId = fromTabId - 1;

                    _s.UpdateCacheId(fromTabId, toTabId);
                }

                //Finally
                tabLayout.removeTabAt(tabNumberToRemove);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        private static void CheckLocalInstance(final Context context) {
            try {
                if (_s == null) {
                    _s = SCommon.GetInstance(context);
                }
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        private static void FullScrollTab(final Context context, final int direction) {
            try {
                tabLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        //noinspection EmptyCatchBlock
                        try {
                            if (direction == HorizontalScrollView.FOCUS_RIGHT) {
                                final int tabId = tabLayout.getTabCount() - 1;
                                //noinspection ConstantConditions
                                tabLayout.getTabAt(tabId).select();
                            } else if (direction == HorizontalScrollView.FOCUS_LEFT) {
                                //noinspection ConstantConditions
                                tabLayout.getTabAt(0).select();
                            }
                            tabLayout.fullScroll(direction);
                        } catch (Exception ex) {
                        }
                    }
                });
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }

        private static void LongPress(final Context context) {
            tabLayout.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        final View vw = tabLayout.getChildAt(0);
                        final int count = ((LinearLayout) vw).getChildCount();
                        for (int i = 0; i < count; i++) {
                            final int index = i;
                            ((LinearLayout) vw).getChildAt(index).setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(final View view) {
                                    //noinspection EmptyCatchBlock
                                    try {
                                        Tab.RemoveTabAt(context, index);
                                        final int tabSelect = (index == 0) ? 0 : index - 1;
                                        //noinspection ConstantConditions
                                        tabLayout.getTabAt(tabSelect).select();
                                        //Focus correction
                                        for (int j = 0; j <= tabSelect; j++) {
                                            tabLayout.arrowScroll(HorizontalScrollView.FOCUS_RIGHT);
                                        }
                                    } catch (Exception ex) {
                                    }

                                    return true;
                                }
                            });
                        }
                    } catch (Exception ex) {
                        if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
                    }
                }
            });
        }

        /***
         * Save ScrollPosY
         */
        static void SaveScrollPosY(final Context context) {
            try {
                CheckLocalInstance(context);

                CacheTabBO t = _s.GetCurrentCacheTab();
                if (t == null) return;

                t.scrollPosY = SearchFragment.GetScrollPosY();
                _s.SaveCacheTab(t);
            } catch (Exception ex) {
                if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
            }
        }
    }

    private void ReloadFavTabTv(final Context context, final String tbbName, final int bNumber, final int cNumber, final String fullQuery, @SuppressWarnings("SameParameterValue") final int vNumber) {
        try {
            if (tabLayout == null)
                return;

            CheckLocalInstance(context);

            final int tabNumber = 0;
            final String bbname = tbbName.substring(0, 1);
            final int pos = 0;
            final CacheTabBO t = new CacheTabBO(tabNumber, "F", context.getString(R.string.tabTitleDefault), fullQuery, pos, bbname, false, false, false, bNumber, cNumber, vNumber, tbbName);
            _s.SaveCacheTab(t);

            final FragmentManager fm = getSupportFragmentManager();
            final FragmentTransaction ft = fm.beginTransaction();
            final Fragment frag = new SearchFragment(SearchFragment.FRAGMENT_TYPE.FAV_TYPE);
            ft.replace(R.id.content_frame, frag, Integer.toString(tabNumber));
            /* Gives exceptions in Android Q
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            */
            ft.commit();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    private void Slide(final boolean showMnu) {
        if (slideViewMenu == null) return;

        if (showMnu) {
            final int installStatus = PCommon.GetInstallStatus(getApplicationContext());
            if (installStatus != 6) {
                PCommon.ShowToast(getApplicationContext(), R.string.installQuit, Toast.LENGTH_SHORT);
                return;
            }
        }

        final int mnuTvVisibility = showMnu ? View.VISIBLE : View.GONE;
        final int tabVisibility = showMnu ? View.GONE : View.VISIBLE;

        slideViewMenu.setVisibility(mnuTvVisibility);
        slideViewTabHandleMain.setVisibility(tabVisibility);
        slideViewTab.setVisibility(tabVisibility);

        if (showMnu) {
            slideViewMenuHandle.requestFocus();
        } else {
            slideViewTabHandle.requestFocus();
        }
    }
/*
final TranslateAnimation animate = new TranslateAnimation(
        0,           // fromXDelta
        -1 * slideViewMenu.getWidth(),          // toXDelta
        0,           // fromYDelta
        0);            // toYDelta
animate.setDuration(500);
animate.setFillAfter(true);
slideViewMenu.startAnimation(animate);
*/

    void SearchDialog(final Context context, final boolean isSearchBible) {
        //TODO FAB: cancel not working
        try {
            final int searchFullQueryLimit = isSearchBible ? 3 : 0;
            final int installStatus = PCommon.GetInstallStatus(context);
            if (installStatus < 1) return;

            final String bibleAppType = PCommon.GetPrefBibleAppType(context);
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final String favSearchText;
            if (isSearchBible) {
                favSearchText = "";
            } else {
                CheckLocalInstance(context);

                final CacheTabBO favCacheTabBO = _s.GetCacheTabFav();
                favSearchText = (favCacheTabBO == null) ? "" : (favCacheTabBO.fullQuery == null) ? "" : favCacheTabBO.fullQuery;
            }

            final String bbname = PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_NAME, "k");
            final AlertDialog builderText = new AlertDialog.Builder(context).create();
            final LayoutInflater inflater = getLayoutInflater();
            final View vw = inflater.inflate(R.layout.fragment_search_dialog, (ViewGroup) findViewById(R.id.clSearch));
            final EditText etSearchText = vw.findViewById(R.id.etSearchText);
            etSearchText.setText(favSearchText);
            etSearchText.setTextSize(fontSize);
            if (typeface != null) etSearchText.setTypeface(typeface);
            final String searchTextHint = PCommon.ConcaT("<i>", getString(isSearchBible ? R.string.searchBibleHint : R.string.searchFavHint, "</i>"));
            etSearchText.setHint(Html.fromHtml(searchTextHint));
            final NumberPicker npSearchLanguage = vw.findViewById(R.id.npSearchLanguage);
            if (bibleAppType.compareTo("1") == 0) npSearchLanguage.setVisibility(View.GONE);
            final TextView tvFavFilter = vw.findViewById(R.id.tvFavFilter);
            PCommon.SetTextAppareance(tvFavFilter, context, R.style.TextAppearance_AppCompat_Headline);
            if (typeface != null) tvFavFilter.setTypeface(typeface);
            final ToggleButton btnFilter0 = vw.findViewById(R.id.btnFavFilter0);
            final ToggleButton btnFilter1 = vw.findViewById(R.id.btnFavFilter1);
            final ToggleButton btnFilter2 = vw.findViewById(R.id.btnFavFilter2);
            final TextView tvFavOrder = vw.findViewById(R.id.tvFavOrder);
            PCommon.SetTextAppareance(tvFavOrder, context, R.style.TextAppearance_AppCompat_Headline);
            if (typeface != null) tvFavOrder.setTypeface(typeface);
            final ToggleButton btnOrder1 = vw.findViewById(R.id.btnFavOrder1);
            final ToggleButton btnOrder2 = vw.findViewById(R.id.btnFavOrder2);

            if (!isSearchBible) {
                npSearchLanguage.setVisibility(View.GONE);
                final int filterBy = PCommon.GetFavFilter(context);
                btnFilter0.setChecked(false);
                btnFilter1.setChecked(false);
                btnFilter2.setChecked(false);
                if (filterBy == 1) btnFilter1.setChecked(true);
                else if (filterBy == 2) btnFilter2.setChecked(true);
                else btnFilter0.setChecked(true);
                btnFilter0.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int filterBy = Integer.parseInt(v.getTag().toString());
                        PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.FAV_FILTER, filterBy);
                        btnFilter1.setChecked(false);
                        btnFilter2.setChecked(false);
                    }
                });
                btnFilter1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int filterBy = Integer.parseInt(v.getTag().toString());
                        PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.FAV_FILTER, filterBy);
                        btnFilter0.setChecked(false);
                        btnFilter2.setChecked(false);
                    }
                });
                btnFilter2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int filterBy = Integer.parseInt(v.getTag().toString());
                        PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.FAV_FILTER, filterBy);
                        btnFilter0.setChecked(false);
                        btnFilter1.setChecked(false);
                    }
                });
                final int orderBy = PCommon.GetFavOrder(context);
                btnOrder1.setChecked(false);
                btnOrder2.setChecked(false);
                if (orderBy == 2) btnOrder2.setChecked(true);
                else btnOrder1.setChecked(true);
                btnOrder1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int orderBy = Integer.parseInt(v.getTag().toString());
                        PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.FAV_ORDER, orderBy);
                        btnOrder2.setChecked(false);
                    }
                });
                btnOrder2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int orderBy = Integer.parseInt(v.getTag().toString());
                        PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.FAV_ORDER, orderBy);
                        btnOrder1.setChecked(false);
                    }
                });
            } else {
                tvFavFilter.setVisibility(View.GONE);
                btnFilter0.setVisibility(View.GONE);
                btnFilter1.setVisibility(View.GONE);
                btnFilter2.setVisibility(View.GONE);

                tvFavOrder.setVisibility(View.GONE);
                btnOrder1.setVisibility(View.GONE);
                btnOrder2.setVisibility(View.GONE);
            }

            final String[] npLanguageValues = new String[]{getString(R.string.languageEn), getString(R.string.languageEs), getString(R.string.languageFrSegondShort), getString(R.string.languageFrOstervaldShort), getString(R.string.languageIt), getString(R.string.languagePt)};
            npSearchLanguage.setDisplayedValues(npLanguageValues);
            npSearchLanguage.setMinValue(1);
            npSearchLanguage.setMaxValue(6);
            npSearchLanguage.setValue(bbname.equalsIgnoreCase("k") ? 1 : bbname.equalsIgnoreCase("v") ? 2 : bbname.equalsIgnoreCase("l") ? 3 : bbname.equalsIgnoreCase("o") ? 4 : bbname.equalsIgnoreCase("d") ? 5 : 6);
            npSearchLanguage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etSearchText.requestFocus();
                }
            });
            final Button btnSearchContinue = vw.findViewById(R.id.btnSearchContinue);
            btnSearchContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etSearchText.setText(etSearchText.getText().toString().replaceAll("\n", ""));
                    if (etSearchText.getText().toString().length() < searchFullQueryLimit) {
                        PCommon.ShowToast(v.getContext(), R.string.toastEmpty3, Toast.LENGTH_SHORT);
                        return;
                    }
                    builderText.dismiss();
                }
            });
            final Button btnSearchClear = vw.findViewById(R.id.btnSearchClear);
            btnSearchClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etSearchText.setText("");
                    etSearchText.requestFocus();
                }
            });
            builderText.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    try {
                        final String bbname = (npSearchLanguage.getValue() == 1) ? "k" : (npSearchLanguage.getValue() == 2) ? "v" : (npSearchLanguage.getValue() == 3) ? "l" : (npSearchLanguage.getValue() == 4) ? "o" : (npSearchLanguage.getValue() == 5) ? "d" : "a";
                        PCommon.SavePref(etSearchText.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbname);
                        SearchTvBook(context, etSearchText.getText().toString(), isSearchBible);
                    } catch (Exception ex) {
                        if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
                    }
                }
            });
            builderText.setTitle(R.string.mnuSearchAll);
            builderText.setCancelable(true);
            builderText.setView(vw);
            builderText.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    private void SearchTvBook(final Context context, final String searchText, final boolean isSearchBible) {
        try {
            final int installStatus = PCommon.GetInstallStatus(context);
            if (installStatus < 1) return;

            if (!isSearchBible) {
                final String bbName = PCommon.GetPrefBibleName(context);
                final String bbname = PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                if (bbname.equals("")) return;
                ShowFav();
                ReloadFavTabTv(context, bbname, 0, 0, searchText, 0);
                return;
            }

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final String bbnm = PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_NAME, "k");

            final AlertDialog builderBook = new AlertDialog.Builder(context).create();
            final LayoutInflater inflater = getLayoutInflater();

            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final ArrayList<BibleRefBO> lstRef = _s.GetListAllBookByName(bbnm);
            final LinearLayout llBooks = new LinearLayout(context);
            llBooks.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llBooks.setOrientation(LinearLayout.VERTICAL);
            llBooks.setPadding(0, 20, 0, 20);

            final AlertDialog builderChapter = new AlertDialog.Builder(context).create();
            final View vwSvSelection = inflater.inflate(R.layout.fragment_selection_items, (ViewGroup) findViewById(R.id.svSelection));

            int bNumber;
            String refText;
            String refNr;
            boolean isBookExist;
            int bNumberParam;
            boolean shouldWarn = false;

            for (BibleRefBO ref : lstRef) {
                bNumber = ref.bNumber;
                refNr = String.format(Locale.US, "%2d", bNumber);
                refText = PCommon.ConcaT(refNr, Html.fromHtml("&nbsp;"), "(", ref.bsName, ") ", ref.bName);

                final TextView tvBook = new TextView(context);
                tvBook.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvBook.setPadding(10, 20, 10, 20);
                tvBook.setText(refText);
                tvBook.setTag(bNumber);

                bNumberParam = (bNumber != 66) ? bNumber + 1 : 66;
                isBookExist = (installStatus == 6) || _s.IsBookExist(bNumberParam);
                if (isBookExist) {
                    tvBook.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View view) {
                            try {
                                final int bNumber = (int) view.getTag();
                                final int chapterMax = _s.GetBookChapterMax(bNumber);
                                if (chapterMax < 1) {
                                    PCommon.ShowToast(view.getContext(), R.string.toastBookNotInstalled, Toast.LENGTH_SHORT);
                                    return;
                                }
                                final String[] titleArr = ((TextView) view).getText().toString().substring(3).split("\\(");
                                final String title = PCommon.ConcaT(getString(R.string.mnuBook), ": ", titleArr[0]);

                                PCommon.SelectItem(builderChapter, view.getContext(), vwSvSelection, title, R.string.tvChapter, "", true, chapterMax, true);
                                builderChapter.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        final String bbName = PCommon.GetPrefBibleName(context);
                                        final String bbname = PCommon.GetPref(view.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                                        if (bbname.equals("")) return;
                                        //not used: final String tbbName = PCommon.GetPrefTradBibleName(view.getContext(), true);
                                        final int cNumber = Integer.parseInt(PCommon.GetPref(view.getContext(), IProject.APP_PREF_KEY.BOOK_CHAPTER_DIALOG, "0"));
                                        final String fullQuery = PCommon.ConcaT(bNumber,
                                                cNumber != 0 ? PCommon.ConcaT(" ", cNumber) : "",
                                                searchText != null ? PCommon.ConcaT(" ", searchText) : "");
                                        //noinspection ConstantConditions
                                        if (isSearchBible) {
                                            MainActivity.Tab.AddTab(view.getContext(), bbname, bNumber, cNumber, fullQuery, 1);
                                        } else {
                                            ShowFav();
                                            ReloadFavTabTv(view.getContext(), bbname, bNumber, cNumber, fullQuery, 0);
                                        }
                                    }
                                });
                                builderChapter.show();
                            } catch (Exception ex) {
                                if (PCommon._isDebugVersion) PCommon.LogR(view.getContext(), ex);
                            } finally {
                                builderBook.dismiss();
                            }
                        }
                    });
                } else {
                    if (!shouldWarn) shouldWarn = true;
                    tvBook.setEnabled(false);
                }
                //TODO FAB: slow GetDrawable
                tvBook.setFocusable(true);
                tvBook.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) tvBook.setTypeface(typeface);
                tvBook.setTextSize(fontSize);

                llBooks.addView(tvBook);
            }

            final Typeface tfTitle = Typeface.defaultFromStyle(Typeface.BOLD);
            final TextView tvNT = new TextView(context);
            tvNT.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvNT.setPadding(10, 60, 10, 20);
            tvNT.setGravity(Gravity.CENTER_HORIZONTAL);
            tvNT.setText(Html.fromHtml(getString(R.string.tvBookNT)));
            PCommon.SetTextAppareance(tvNT, context, R.style.TextAppearance_AppCompat_Headline);
            if (tfTitle != null) tvNT.setTypeface(tfTitle);
            llBooks.addView(tvNT, 39);

            final TextView tvOT = new TextView(context);
            tvOT.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvOT.setPadding(10, 20, 10, 20);
            tvOT.setGravity(Gravity.CENTER_HORIZONTAL);
            tvOT.setText(Html.fromHtml(getString(R.string.tvBookOT)));
            PCommon.SetTextAppareance(tvOT, context, R.style.TextAppearance_AppCompat_Headline);
            if (tfTitle != null) tvOT.setTypeface(tfTitle);
            llBooks.addView(tvOT, 0);

            final String refNr0 = String.format(Locale.US, "%2d", 0);
            final String refText0 = PCommon.ConcaT(refNr0, ": ", context.getString(R.string.itemAll));
            final TextView tvALL = new TextView(context);
            tvALL.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvALL.setPadding(10, 20, 10, 20);
            tvALL.setText(refText0);
            if (typeface != null) tvALL.setTypeface(typeface);

            tvALL.setTextSize(fontSize);
            //TODO FAB: slow GetDrawable
            tvALL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String bbName = PCommon.GetPrefBibleName(context);
                    final String bbname = PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                    if (bbname.equals("")) return;
                    //noinspection ConstantConditions
                    if (isSearchBible) {
                        MainActivity.Tab.AddTab(v.getContext(), "S", bbname, searchText);
                    } else {
                        ShowFav();
                        ReloadFavTabTv(v.getContext(), bbname, 0, 0, searchText, 0);
                    }
                    builderBook.dismiss();
                }
            });
            tvALL.setFocusable(true);
            tvALL.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
            llBooks.addView(tvALL, 0);

            if (shouldWarn) {
                final TextView tvWarn = new TextView(context);
                tvWarn.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvWarn.setPadding(10, 10, 10, 20);
                tvWarn.setGravity(Gravity.CENTER_HORIZONTAL);
                tvWarn.setText(R.string.tvBookInstall);
                tvWarn.setTextSize(fontSize);
                llBooks.addView(tvWarn, 0);
            }
            sv.addView(llBooks);

            builderBook.setTitle(R.string.mnuBooks);
            builderBook.setCancelable(true);
            builderBook.setView(sv);
            builderBook.show();
        } catch (Exception ex) {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }
}
