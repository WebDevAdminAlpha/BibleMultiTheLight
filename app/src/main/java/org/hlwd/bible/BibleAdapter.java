
package org.hlwd.bible;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

class BibleAdapter extends RecyclerView.Adapter<BibleAdapter.ViewHolder>
{
    @SuppressWarnings("UnusedAssignment")
    ArrayList<VerseBO> lstVerse = null;
    private String markFav;
    private String markReading;
    private int prefix = 0;             //TODO NEXT: get prefix
    private SCommon _s = null;

    //Used for cross_references
    private int bNumber;
    private int cNumber;
    private int vNumber;
    private SEARCH_TYPE searchType;

    private enum SEARCH_TYPE
    {
        CROSS_REFERENCE,
        OTHER
    }

    private void SetMark(final Context context)
    {
        this.markFav = PCommon.GetPref(context, IProject.APP_PREF_KEY.FAV_SYMBOL, context.getString(R.string.favSymbolFavDefault));
        this.markReading = context.getString(R.string.favSymbolReadingDefault);
    }

    BibleAdapter()
    {
        this.lstVerse = null;
    }

    BibleAdapter(final Context context, final String tbbName, final int bNumber, final int cNumber, final int vNumber)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;

        this.lstVerse = _s.GetVerse(tbbName, bNumber, cNumber, vNumber);
    }

    BibleAdapter(final Context context, final String tbbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;

        this.lstVerse = _s.GetVerses(tbbName, bNumber, cNumber, vNumberFrom, vNumberTo);

        this.SaveCacheSearch(context);
    }

    BibleAdapter(final Context context, final String tbbName, final int bNumber, final int cNumber, final int vNumber, final String searchString)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.bNumber = bNumber;
        this.cNumber = cNumber;
        this.vNumber = vNumber;
        this.searchType = SEARCH_TYPE.CROSS_REFERENCE;

        this.lstVerse = _s.GetCrossReferences(tbbName, bNumber, cNumber, vNumber);
    }

    BibleAdapter(final Context context, final String bbName, final int bNumber, final int cNumber, final String searchString)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;

        this.lstVerse = _s.SearchBible(bbName, bNumber, cNumber, searchString);

        this.SaveCacheSearch(context);
    }

    BibleAdapter(final Context context, final String tbbName, final int bNumber, final int cNumber)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;

        this.lstVerse = _s.GetChapter(tbbName, bNumber, cNumber);
    }

    BibleAdapter(final Context context, final String bbName, final int bNumber, final String searchString)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;

        this.lstVerse = _s.SearchBible(bbName, bNumber, searchString);

        this.SaveCacheSearch(context);
    }

    BibleAdapter(final Context context, final String bbName, final String searchString)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;

        this.lstVerse = _s.SearchBible(bbName, searchString);

        this.SaveCacheSearch(context);
    }

    BibleAdapter(final Context context, final int searchId)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;

        this.lstVerse = _s.SearchBible(searchId);
    }

    /***
     * Get all favorites
     * @param context
     * @param bbName
     * @param searchString  Give NULL to get all notes
     * @param orderBy       Order by
     * @param markType      Mark type
     */
    @SuppressWarnings("JavaDoc")
    BibleAdapter(final Context context, final String bbName, final String searchString, final int orderBy, final int markType)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;

        this.lstVerse = _s.SearchFav(bbName, searchString, orderBy, markType);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        //private LinearLayout card_recipient;
        private final CardView cv;
        private final TextView tv_ref;
        private final TextView tv_text;
        private final TextView tv_mark;
        private final TextView tv_cr;

        ViewHolder(View view)
        {
            super(view);

            cv = view.findViewById(R.id.cv);
            tv_ref = view.findViewById(R.id.tv_ref);
            tv_text = view.findViewById(R.id.tv_text);
            tv_cr = view.findViewById(R.id.tv_cr);
            tv_mark = view.findViewById(R.id.tv_mark);

            final Typeface typeface = PCommon.GetTypeface(view.getContext());
            if (typeface != null)
            {
                tv_ref.setTypeface(typeface, Typeface.BOLD);
                tv_text.setTypeface(typeface);
                tv_cr.setTypeface(typeface);
            }
            final int fontSize = PCommon.GetFontSize(view.getContext());
            if (tv_ref != null) tv_ref.setTextSize(fontSize);
            tv_text.setTextSize(fontSize);
            tv_cr.setTextSize(fontSize);
            tv_mark.setTextSize(fontSize);
        }
    }

    @NonNull
    @Override
    public BibleAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int viewType)
    {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(prefix == 0
                                                ? R.layout.card_recipient
                                                : R.layout.card_recipient1,
                                                viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BibleAdapter.ViewHolder viewHolder, final int position)
    {
        //Current verse
        final VerseBO verse = lstVerse.get(position);
        final String ref = prefix == 0
                ? PCommon.ConcaT(verse.bName, " ", verse.cNumber, ".", verse.vNumber)
                : PCommon.ConcaT(verse.vNumber);

        final String crCount = verse.crCount > 0 ? String.valueOf(verse.crCount) : "";

        //Mark
        switch (verse.mark)
        {
            case 0:
                break;

            case 1:
                viewHolder.tv_mark.setPadding(10, 0, 5, 0);
                viewHolder.tv_mark.setText( markFav );
                break;

            case 2:
                viewHolder.tv_mark.setPadding(10, 0, 5, 0);
                viewHolder.tv_mark.setText( markReading );
                break;
        }

        if (viewHolder.tv_ref != null)
        {
            viewHolder.tv_ref.setText(ref);
            viewHolder.tv_ref.setId(verse.id);
            viewHolder.tv_ref.setTag(position);
        }

        if (prefix == 0)
        {
            viewHolder.tv_text.setText(verse.vText);
        }
        else if (prefix == 1)
        {
            viewHolder.tv_text.setText(PCommon.ConcaT(verse.vNumber, ". ", verse.vText));
        }
        viewHolder.tv_text.setId(verse.id);
        viewHolder.tv_text.setTag(position);

        viewHolder.tv_cr.setText(crCount);

        if (this.searchType == SEARCH_TYPE.CROSS_REFERENCE && verse.bNumber == this.bNumber && verse.cNumber == this.cNumber && verse.vNumber == this.vNumber)
        {
            final int fgColor = viewHolder.tv_text.getResources().getColor(R.color.white);
            viewHolder.cv.setCardBackgroundColor(viewHolder.cv.getResources().getColor(R.color.blueDark));
            viewHolder.tv_mark.setTextColor(fgColor);
            if (viewHolder.tv_ref != null) viewHolder.tv_ref.setTextColor(fgColor);
            viewHolder.tv_text.setTextColor(fgColor);
            viewHolder.tv_cr.setTextColor(fgColor);
            viewHolder.setIsRecyclable(false);
        }

        //Events
        if (viewHolder.tv_ref != null)
        {
            viewHolder.tv_ref.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View view)
                {
                    final TextView tvRef = (TextView) view;
                    if (tvRef == null) return false;

                    final int bibleId = tvRef.getId();
                    final int position = Integer.parseInt( tvRef.getTag().toString() );
                    PCommon.SavePrefInt(view.getContext(), IProject.APP_PREF_KEY.BIBLE_ID, bibleId);
                    PCommon.SavePrefInt(view.getContext(), IProject.APP_PREF_KEY.VIEW_POSITION, position);

                    return false;
                }
            });
        }
        viewHolder.tv_text.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                final TextView tvText = (TextView) view;
                if (tvText == null) return false;

                final int bibleId = tvText.getId();
                final int position = Integer.parseInt( tvText.getTag().toString() );
                PCommon.SavePrefInt(view.getContext(), IProject.APP_PREF_KEY.BIBLE_ID, bibleId);
                PCommon.SavePrefInt(view.getContext(), IProject.APP_PREF_KEY.VIEW_POSITION, position);

                return false;
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return lstVerse == null ? 0 : lstVerse.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        final VerseBO verse = lstVerse.get(position);

        return verse.mark;
    }

    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null)
            {
                _s = SCommon.GetInstance(context);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }

    private void SaveCacheSearch(final Context context)
    {
        try
        {
            ArrayList<Integer> lstId = new ArrayList<>();

            if (lstVerse != null)
            {
                for (VerseBO verse : lstVerse) {
                    lstId.add(verse.id);
                }
            }

            _s.SaveCacheSearch(lstId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }
}
